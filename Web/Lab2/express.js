var express = require('express'); 
var app = express(); 
 
// получение GET запроса на главную страницу 
app.get('/', function (req, res) { 
  res.send('Got a Get request'); 
}) 
 
// получение POST запроса на главную страницу 
app.post('/', function (req, res) { 
  res.send('Got a POST request'); 
}) 
 
// получение PUT запроса по адресу /user 
app.put('/user', function (req, res) { 
  res.send('Got a PUT request at /user'); 
}) 
 
// получение DELETE запроса по адресу /user 
app.delete('/user', function (req, res) { 
  res.send('Got a DELETE request at /user'); 
}) 
 
var server = app.listen(8080, function () { 
 var host = server.address().address 
 var port = server.address().port 
 console.log('Example app listening at http://%s:%s', host, port) 
});