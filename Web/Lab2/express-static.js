var express = require('express'); 
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser')
 
// получение GET запроса на главную страницу 
app.get('/', function (req, res) { 
    var options = { 
        root: './', 
        dotfiles: 'deny', 
        headers: { 
            'x-timestamp': Date.now(), 
            'x-sent': true 
        } 
    }; 

    var fileName = "static/html/hack.html";
    res.sendFile(fileName, options, function (err) { 
        if (err) { 
            console.log(err); 
            res.status(err.status).end(); 
        } else { 
            console.log('Sent:', fileName); 
        } 
    });
}) 

// получение GET запроса на главную страницу 
app.get('/hack', function (req, res) { 
    var options = { 
        root: './', 
        dotfiles: 'deny', 
        headers: { 
            'x-timestamp': Date.now(), 
            'x-sent': true 
        } 
    }; 

    var fileName = "static/html/index.html";
    res.sendFile(fileName, options, function (err) { 
        if (err) { 
            console.log(err); 
            res.status(err.status).end(); 
        } else { 
            console.log('Sent:', fileName); 
        } 
    });
}) 

// получение GET запроса на /img 
app.get('/img', function (req, res) { 
    var options = { 
        root: './', 
        dotfiles: 'deny', 
        headers: { 
            'x-timestamp': Date.now(), 
            'x-sent': true 
        } 
    }; 

    var fileName = "static/img/picture.jpg";
    res.sendFile(fileName, options, function (err) { 
        if (err) { 
            console.log(err); 
            res.status(err.status).end(); 
        } else { 
            console.log('Sent:', fileName); 
        } 
    });
})

// получение GET запроса на /txt
app.get('/txt', function (req, res) { 
    var options = { 
        root: './', 
        dotfiles: 'deny', 
        headers: {
            'x-timestamp': Date.now(), 
            'x-sent': true
        }
    }; 
 
    // TXT Fix
    var fileName = "static/html/hack.txt";
    fs.readFile(fileName, 'utf8', function(err, data) { 
        if (err){ 
            console.log('Could not find or open file for reading'); 
        } else { 
            res.writeHead(200, {'Content-Type': 'text/html'}); 
            res.end(data); 
        } 
    })
})

// Обработка POST
var textParser = bodyParser.text();
app.post('/message', textParser, function (req, res) { 
    res.send('Got a POST request');
    if (!req.body) return res.sendStatus(400);
    fs.writeFile("post-data.txt", req.body, function(err) {
        if (err) {
            console.log('Could not write file');
        } else {
            console.log('POST data succesfully written');
        }
    });
}) 
 
// получение PUT запроса по адресу /user 
app.put('/user', function (req, res) { 
    res.send('Got a PUT request at /user'); 
}) 
 
// получение DELETE запроса по адресу /user 
app.delete('/user', function (req, res) { 
    res.send('Got a DELETE request at /user'); 
}) 
 
var server = app.listen(8080, function () { 
    var host = server.address().address 
    var port = server.address().port 
    console.log('Example app listening at http://%s:%s', host, port) 
});