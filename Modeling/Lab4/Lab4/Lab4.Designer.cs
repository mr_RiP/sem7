﻿namespace Lab4
{
    partial class Lab4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.tbLambda = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbK = new System.Windows.Forms.TextBox();
            this.btnErlang = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFrom = new System.Windows.Forms.TextBox();
            this.tbTo = new System.Windows.Forms.TextBox();
            this.btnUniform = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lbPassed = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbDropped = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbRecycled = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbCurrTime = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbAverageSize = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbFrameInterval = new System.Windows.Forms.TextBox();
            this.btnFrameInterval = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnQueue = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbMaxSize = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbDeltaT = new System.Windows.Forms.TextBox();
            this.tbReturnProbability = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "λ:";
            // 
            // tbLambda
            // 
            this.tbLambda.Location = new System.Drawing.Point(37, 32);
            this.tbLambda.Name = "tbLambda";
            this.tbLambda.Size = new System.Drawing.Size(127, 20);
            this.tbLambda.TabIndex = 2;
            this.tbLambda.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "k:";
            // 
            // tbK
            // 
            this.tbK.Location = new System.Drawing.Point(37, 58);
            this.tbK.Name = "tbK";
            this.tbK.Size = new System.Drawing.Size(127, 20);
            this.tbK.TabIndex = 4;
            this.tbK.Text = "1";
            // 
            // btnErlang
            // 
            this.btnErlang.Location = new System.Drawing.Point(10, 84);
            this.btnErlang.Name = "btnErlang";
            this.btnErlang.Size = new System.Drawing.Size(154, 23);
            this.btnErlang.TabIndex = 5;
            this.btnErlang.Text = "Задать";
            this.btnErlang.UseVisualStyleBackColor = true;
            this.btnErlang.Click += new System.EventHandler(this.btnErlang_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Равномерное распределение";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "От:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "До:";
            // 
            // tbFrom
            // 
            this.tbFrom.Location = new System.Drawing.Point(37, 32);
            this.tbFrom.Name = "tbFrom";
            this.tbFrom.Size = new System.Drawing.Size(127, 20);
            this.tbFrom.TabIndex = 9;
            this.tbFrom.Text = "0";
            // 
            // tbTo
            // 
            this.tbTo.Location = new System.Drawing.Point(37, 57);
            this.tbTo.Name = "tbTo";
            this.tbTo.Size = new System.Drawing.Size(127, 20);
            this.tbTo.TabIndex = 10;
            this.tbTo.Text = "1";
            // 
            // btnUniform
            // 
            this.btnUniform.Location = new System.Drawing.Point(10, 83);
            this.btnUniform.Name = "btnUniform";
            this.btnUniform.Size = new System.Drawing.Size(154, 23);
            this.btnUniform.TabIndex = 11;
            this.btnUniform.Text = "Задать";
            this.btnUniform.UseVisualStyleBackColor = true;
            this.btnUniform.Click += new System.EventHandler(this.btnUniform_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(21, 446);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(137, 23);
            this.btnStart.TabIndex = 20;
            this.btnStart.Text = "Начать";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(164, 446);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(137, 23);
            this.btnStop.TabIndex = 21;
            this.btnStop.Text = "Остановить";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(307, 446);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(137, 23);
            this.btnClear.TabIndex = 23;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(34, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Заявок обработано:";
            // 
            // lbPassed
            // 
            this.lbPassed.AutoSize = true;
            this.lbPassed.Location = new System.Drawing.Point(199, 29);
            this.lbPassed.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbPassed.Name = "lbPassed";
            this.lbPassed.Size = new System.Drawing.Size(0, 13);
            this.lbPassed.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(34, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Заявок пропущено:";
            // 
            // lbDropped
            // 
            this.lbDropped.AutoSize = true;
            this.lbDropped.Location = new System.Drawing.Point(199, 55);
            this.lbDropped.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbDropped.Name = "lbDropped";
            this.lbDropped.Size = new System.Drawing.Size(0, 13);
            this.lbDropped.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 129);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(140, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Текущий размер очереди:";
            // 
            // lbSize
            // 
            this.lbSize.AutoSize = true;
            this.lbSize.Location = new System.Drawing.Point(199, 129);
            this.lbSize.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(0, 13);
            this.lbSize.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Заявок вернулось в очередь:";
            // 
            // lbRecycled
            // 
            this.lbRecycled.AutoSize = true;
            this.lbRecycled.Location = new System.Drawing.Point(199, 80);
            this.lbRecycled.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbRecycled.Name = "lbRecycled";
            this.lbRecycled.Size = new System.Drawing.Size(0, 13);
            this.lbRecycled.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(34, 106);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Времени прошло:";
            // 
            // lbCurrTime
            // 
            this.lbCurrTime.AutoSize = true;
            this.lbCurrTime.Location = new System.Drawing.Point(199, 106);
            this.lbCurrTime.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbCurrTime.Name = "lbCurrTime";
            this.lbCurrTime.Size = new System.Drawing.Size(0, 13);
            this.lbCurrTime.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(34, 155);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(138, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Средний размер очереди:";
            // 
            // lbAverageSize
            // 
            this.lbAverageSize.AutoSize = true;
            this.lbAverageSize.Location = new System.Drawing.Point(199, 155);
            this.lbAverageSize.MaximumSize = new System.Drawing.Size(200, 0);
            this.lbAverageSize.Name = "lbAverageSize";
            this.lbAverageSize.Size = new System.Drawing.Size(0, 13);
            this.lbAverageSize.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Интервал отображения:";
            // 
            // tbFrameInterval
            // 
            this.tbFrameInterval.Location = new System.Drawing.Point(143, 20);
            this.tbFrameInterval.Name = "tbFrameInterval";
            this.tbFrameInterval.Size = new System.Drawing.Size(100, 20);
            this.tbFrameInterval.TabIndex = 37;
            this.tbFrameInterval.Text = "500000";
            // 
            // btnFrameInterval
            // 
            this.btnFrameInterval.Location = new System.Drawing.Point(9, 48);
            this.btnFrameInterval.Name = "btnFrameInterval";
            this.btnFrameInterval.Size = new System.Drawing.Size(234, 23);
            this.btnFrameInterval.TabIndex = 38;
            this.btnFrameInterval.Text = "Задать";
            this.btnFrameInterval.UseVisualStyleBackColor = true;
            this.btnFrameInterval.Click += new System.EventHandler(this.btnFrameInterval_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Распределение Эргланга";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbLambda);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbK);
            this.groupBox1.Controls.Add(this.btnErlang);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 116);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "События на вход";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbFrom);
            this.groupBox2.Controls.Add(this.tbTo);
            this.groupBox2.Controls.Add(this.btnUniform);
            this.groupBox2.Location = new System.Drawing.Point(12, 134);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 116);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "События на выход";
            // 
            // btnQueue
            // 
            this.btnQueue.Location = new System.Drawing.Point(9, 96);
            this.btnQueue.Name = "btnQueue";
            this.btnQueue.Size = new System.Drawing.Size(234, 23);
            this.btnQueue.TabIndex = 19;
            this.btnQueue.Text = "Задать";
            this.btnQueue.UseVisualStyleBackColor = true;
            this.btnQueue.Click += new System.EventHandler(this.btnQueue_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Максимальный размер:";
            // 
            // tbMaxSize
            // 
            this.tbMaxSize.Location = new System.Drawing.Point(143, 17);
            this.tbMaxSize.Name = "tbMaxSize";
            this.tbMaxSize.Size = new System.Drawing.Size(100, 20);
            this.tbMaxSize.TabIndex = 14;
            this.tbMaxSize.Text = "10";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Изменение времени:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Шанс возврата:";
            // 
            // tbDeltaT
            // 
            this.tbDeltaT.Location = new System.Drawing.Point(143, 44);
            this.tbDeltaT.Name = "tbDeltaT";
            this.tbDeltaT.Size = new System.Drawing.Size(100, 20);
            this.tbDeltaT.TabIndex = 17;
            this.tbDeltaT.Text = "0.01";
            // 
            // tbReturnProbability
            // 
            this.tbReturnProbability.Location = new System.Drawing.Point(143, 70);
            this.tbReturnProbability.Name = "tbReturnProbability";
            this.tbReturnProbability.Size = new System.Drawing.Size(100, 20);
            this.tbReturnProbability.TabIndex = 18;
            this.tbReturnProbability.Text = "0.1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.tbMaxSize);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.tbDeltaT);
            this.groupBox3.Controls.Add(this.tbReturnProbability);
            this.groupBox3.Controls.Add(this.btnQueue);
            this.groupBox3.Location = new System.Drawing.Point(198, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(251, 127);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры очереди";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.tbFrameInterval);
            this.groupBox4.Controls.Add(this.btnFrameInterval);
            this.groupBox4.Location = new System.Drawing.Point(198, 169);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(251, 81);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Настройки симуляции";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.lbPassed);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.lbDropped);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.lbAverageSize);
            this.groupBox5.Controls.Add(this.lbSize);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.lbCurrTime);
            this.groupBox5.Controls.Add(this.lbRecycled);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(12, 261);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(437, 179);
            this.groupBox5.TabIndex = 43;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Результаты";
            // 
            // Lab4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 478);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Lab4";
            this.Text = "Lab 4";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbLambda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbK;
        private System.Windows.Forms.Button btnErlang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFrom;
        private System.Windows.Forms.TextBox tbTo;
        private System.Windows.Forms.Button btnUniform;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbPassed;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbDropped;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbRecycled;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbCurrTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbAverageSize;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbFrameInterval;
        private System.Windows.Forms.Button btnFrameInterval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnQueue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbMaxSize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDeltaT;
        private System.Windows.Forms.TextBox tbReturnProbability;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
    }
}

