﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer.EventGenerators
{
    public class ErlangEventGenerator : EventGenerator
    {
        private double _lambda;
        private int _k;

        public ErlangEventGenerator(double lambda, int k)
        {
            Lambda = lambda;
            K = k;
        }

        public double Lambda
        {
            get
            {
                return _lambda;
            }

            set
            {
                if (value <= 0.0)
                    throw new ArgumentOutOfRangeException();

                _lambda = value;
            }
        }

        public int K
        {
            get
            {
                return _k;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException();

                _k = value;
            }
        }

        public override double NextEvent(double currTime)
        {
            return currTime + ErlangNext();
        }

        private double ErlangNext()
        {
            double[] uniformValues = NextSequence(double.Epsilon, 1.0, _k);

            double uniformMultiplication = 1;
            foreach (double value in uniformValues)
                uniformMultiplication *= value;

            return -1.0 / _lambda * Math.Log(uniformMultiplication);
        }
    }
}
