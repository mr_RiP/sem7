﻿using Common;
using QueueAnalyzer.EventGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    sealed class EventRunner
    {
        public EventGenerator Input
        {
            get
            {
                return input;
            }
            set
            {
                if (value != null)
                    input = value;
                else
                    throw new ArgumentNullException("Input");
            }
        }
        private EventGenerator input;

        public EventGenerator Output
        {
            get
            {
                return output;
            }
            set
            {
                if (value != null)
                    output = value;
                else
                    throw new ArgumentNullException("Output");
            }
        }
        private EventGenerator output;

        public int MaxSize
        {
            get
            {
                return maxSize;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("MaxSize");

                maxSize = value;
                if (size > MaxSize)
                {
                    dropped += size - MaxSize;
                    size = MaxSize;
                }
            }
        }
        private int maxSize;

        public double Interval
        {
            get
            {
                return interval;
            }
            set
            {
                if (value < 0 || value.Compare(0))
                    throw new ArgumentOutOfRangeException("interval");
                interval = value;
            }
        }
        private double interval;

        public double ReturnProbability
        {
            get
            {
                return returnProbability;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("ReturnProbability");
                returnProbability = value;
            }
        }
        private double returnProbability;

        public long Iterations { get; private set; } = 0;

        private long sumSize = 0;

        private long size = 0;

        private long dropped  = 0;

        private long passed = 0;

        private long recycled = 0;

        private double currTime = 0;

        private double nextInput;

        private double? nextOutput = null;

        public EventRunner(EventGenerator input, EventGenerator output, double returnProbability, int maxSize, double interval)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (output == null)
                throw new ArgumentNullException("output");
            if (returnProbability < 0)
                throw new ArgumentOutOfRangeException("returnProbability");
            if (maxSize <= 0)
                throw new ArgumentOutOfRangeException("queueSize");
            if (interval < 0 || interval.Compare(0))
                throw new ArgumentOutOfRangeException("interval");

            this.input = input;
            this.output = output;
            this.returnProbability = returnProbability;
            this.MaxSize = maxSize;
            this.interval = interval;

            this.nextInput = input.NextEvent(0);
        }

        public void Step()
        {
            if (size != 0 && !nextOutput.HasValue)
                nextOutput = output.NextEvent(currTime);
            if (currTime >= nextOutput)
            {
                --size;
                ++passed;
                nextOutput = null;
                if (EventGenerator.Next() < ReturnProbability)
                {
                    ++size;
                    ++recycled;
                }
            }
            if (currTime >= nextInput)
            {
                if (size < MaxSize)
                    ++size;
                else
                    ++dropped;
                nextInput = input.NextEvent(currTime);
            }
            currTime += Interval;
            sumSize += size;
            ++Iterations;
        }

        public QueueStats GetStats()
        {
            return new QueueStats(passed, size, dropped, recycled, currTime, (Iterations != 0) ? ((double?) sumSize / Iterations) : null);
        }
    }
}
