﻿using Lab1.RandomValueDist;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (TextValuesCheck())
                Draw();
        }

        private bool TextValuesCheck()
        {
            if (EmptyFieldsCheck())
                return ValuesCheck();
            return false;
        }

        private bool ValuesCheck()
        {                
            double xMin = Double.Parse(XMinText.Text);
            double xMax = Double.Parse(XMaxText.Text);
            if (xMin >= xMax)
            {
                MessageBox.Show("Значение x min должно быть меньше, чем значение x max", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            double step = Double.Parse(StepText.Text);
            if (step > (xMax - xMin))
            {
                MessageBox.Show("Значение шага не должно быть больше длины отрезка (x min, x max)", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int segments = Int32.Parse(SegmentsText.Text);
            if (segments == 0 || segments % 2 != 0)
            {
                MessageBox.Show("Значение количества отрезков N в формуле Котеса должно быть целым четным положительным числом", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            double a = Double.Parse(AText.Text);
            double b = Double.Parse(BText.Text);
            if (a >= b)
            {
                MessageBox.Show("Значение a должно быть меньше, чем значение b", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;

        }

        private bool EmptyFieldsCheck()
        {
            if (String.IsNullOrEmpty(AText.Text) || String.IsNullOrEmpty(BText.Text) ||
                String.IsNullOrEmpty(LambdaText.Text) || String.IsNullOrEmpty(GammaText.Text) || String.IsNullOrEmpty(GammaText.Text) ||
                String.IsNullOrEmpty(XMaxText.Text) || String.IsNullOrEmpty(XMinText.Text) || String.IsNullOrEmpty(StepText.Text))
            {
                MessageBox.Show("Поля не могут быть пустыми", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
                return true;
        }

        private void Draw()
        {
            var graphics = new List<RandomValuesGraphics>
            {
                new RandomValuesGraphics(new UniformDist(Double.Parse(AText.Text), Double.Parse(BText.Text)),
                    Chart.Series["UniDistribution"], Chart.Series["UniDensity"]),
                new RandomValuesGraphics(new ErlangDist(Double.Parse(LambdaText.Text), Int32.Parse(GammaText.Text), Int32.Parse(SegmentsText.Text)),
                    Chart.Series["ErlangDistribution"],Chart.Series["ErlangDensity"]),
            };

            double xMin = Double.Parse(XMinText.Text);
            double xMax = Double.Parse(XMaxText.Text);
            double step = Double.Parse(StepText.Text);
            foreach (var item in graphics)
                item.Draw(xMin, xMax, step);
            
            foreach (var area in Chart.ChartAreas)
            {
                area.AxisX.Minimum = xMin;
                area.AxisX.Maximum = xMax;
            }
            
        }

        private void TextBoxNumericValueFilter(TextBox sender, KeyPressEventArgs e, bool AllowNegative = true, bool AllowDecimal = true)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ',') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && (!AllowDecimal || (sender.Text.IndexOf(',') > -1)))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '-') && (!AllowNegative || (sender.Text.IndexOf('-') > -1) || (sender.SelectionStart != 0)))
            {
                e.Handled = true;
            }
        }

        private void AText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e);
        }

        private void StepText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e, false);
        }

        private void BText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e);
        }

        private void LambdaText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e, false);
        }

        private void XMaxText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e);
        }

        private void XMinText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e);
        }

        private void GammaText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e, false, false);
        }

        private void SegmentsText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBoxNumericValueFilter(sender as TextBox, e, false, false);
        }
    }
}
