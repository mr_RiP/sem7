﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.RandomValueDist
{
    interface IRandomValueDist
    {
        double Distribution(double x);
        double Density(double x);
    }
}
