﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.RandomValueDist
{
    class ExponentialDist : IRandomValueDist
    {
        public ExponentialDist(double lambda)
        {
            if (lambda <= 0)
                throw new ArgumentException("lambda должна быть больше 0");

            _lambda = lambda;
        }

        private double _lambda;

        public double Lambda
        {
            get
            {
                return _lambda;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentException("lambda должна быть больше 0");

                _lambda = value;
            }
        }

        public double Density(double x)
        {
            return x < 0.0 ? 0.0 : _lambda * Math.Exp(-_lambda * x);
        }

        public double Distribution(double x)
        {
            return x < 0.0 ? 0.0 : 1.0 - Math.Exp(-_lambda * x);
        }
    }
}
