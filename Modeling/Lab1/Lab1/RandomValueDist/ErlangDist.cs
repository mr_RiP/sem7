﻿using Lab1.RandomValueDist;
using System;

namespace Lab1.RandomValueDist
{
    class ErlangDist : IRandomValueDist
    {
        public ErlangDist(double lambda, int gamma, int segCount)
        {
            Gamma = gamma;
            Lambda = lambda;
            SegmentsCount = segCount;
        }


        private double _lambda;
        private double _lambdaPow;
        private int _gamma;
        private double _gammaFunc;
        private int _segCount;

        public int SegmentsCount
        {
            get
            {
                return _segCount;
            }

            set
            {
                if (value <= 0 || value % 2 != 0)
                    throw new ArgumentException("Количество отрезков должно четным положительным числом");

                _segCount = value;
            }
        }

        public double Lambda
        {
            get
            {
                return _lambda;
            }

            set
            {
                if (value <= 0.0)
                    throw new ArgumentException("Lambda должна быть больше 0");

                _lambda = value;
                _lambdaPow = Math.Pow(_lambda, _gamma);
            }
        }

        public int Gamma
        {
            get
            {
                return _gamma;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentException("Gamma должна быть больше 0");

                _gamma = value;
                _lambdaPow = Math.Pow(_lambda, _gamma);
                SetGammaFunction();
            }
        }

        private void SetGammaFunction()
        {
            int n = _gamma - 1;
            double res = 1.0;
            for (int i = 2; i <= n; ++i)
                res *= (double)i;
            _gammaFunc = res;
        }

        public double Distribution(double x)
        {
            double step = x / (double)_segCount;
            int n = _segCount / 2;

            double sum1 = 0.0;
            for (int i = 1; i <= n - 1; i++)
                sum1 += Density(2.0 * (double)i * step);
            sum1 *= 2.0;

            double sum2 = 0.0;
            for (int i = 1; i <= n; i++)
                sum2 += Density((double)(2 * i - 1) * step);
            sum2 *= 4.0;

            return step / 3.0 * (sum1 + sum2 + Density(x)); 

        }

        public double Density(double x)
        {
            return x < 0.0 ? 0.0 : _lambdaPow * Math.Pow(x, _gamma - 1) / Math.Exp(_lambda * x) / _gammaFunc;
        }
    }
}