﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.RandomValueDist
{
    class UniformDist : IRandomValueDist
    {
        public UniformDist(double a, double b)
        {
            if (a >= b)
                throw new ArgumentException("a должно быть меньше b");

            _a = a;
            _b = b;
        }

        private double _a;
        private double _b;

        public double A
        {
            get
            {
                return _a;
            }

            set
            {
                if (value >= _b)
                    throw new ArgumentException("a должно быть меньше b");
                _a = value;
            }
        }

        public double B
        {
            get
            {
                return _b;
            }

            set
            {
                if (value <= _a)
                    throw new ArgumentException("b должно быть больше a");
                _b = value;
            }
        }

        public double Density(double x)
        {
            return _a <= x && x <= _b ? 1.0 / (_b - _a) : 0.0;
        }

        public double Distribution(double x)
        {
            return x < _a ? 0.0 : x < _b ? (x - _a) / (_b - _a) : 1.0;
        }
    }
}
