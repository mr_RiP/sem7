﻿using Lab1.RandomValueDist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab1
{
    class RandomValuesGraphics
    {
        public RandomValuesGraphics(IRandomValueDist val, Series distSeries, Series densitySeries)
        {
            _val = val ?? throw new ArgumentNullException("val", "Cлучайная величины не должен быть NULL");
            _dist = distSeries ?? throw new ArgumentNullException("distSeries", "График распределения случайной величины не должен быть NULL");
            _density = densitySeries ?? throw new ArgumentNullException("densitySeries", "График плотности случайной величины не должен быть NULL");
        }

        private IRandomValueDist _val;
        private Series _dist;
        private Series _density;

        public void Draw(double xMin, double xMax, double step)
        {
            if (xMin > xMax)
                throw new ArgumentException("xMin должен быть меньше или равен xMax");

            if (step <= 0.0)
                throw new ArgumentException("step должен быть больше нуля");

            _dist.Points.Clear();
            _density.Points.Clear();

            for (double x = xMin; x <= xMax; x += step)
            {
                _dist.Points.AddXY(x, _val.Distribution(x));
                _density.Points.AddXY(x, _val.Density(x));
            }
        }
    }
}
