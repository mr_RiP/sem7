﻿namespace Lab1
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.StartButton = new System.Windows.Forms.Button();
            this.UniformDistGroup = new System.Windows.Forms.GroupBox();
            this.BText = new System.Windows.Forms.TextBox();
            this.AText = new System.Windows.Forms.TextBox();
            this.BLabel = new System.Windows.Forms.Label();
            this.ALabel = new System.Windows.Forms.Label();
            this.ErlangDistGroup = new System.Windows.Forms.GroupBox();
            this.SegmentsText = new System.Windows.Forms.TextBox();
            this.SegmentsLabel = new System.Windows.Forms.Label();
            this.GammaText = new System.Windows.Forms.TextBox();
            this.GammaLabel = new System.Windows.Forms.Label();
            this.LambdaText = new System.Windows.Forms.TextBox();
            this.LambdaLabel = new System.Windows.Forms.Label();
            this.CommonOptionsGroup = new System.Windows.Forms.GroupBox();
            this.StepText = new System.Windows.Forms.TextBox();
            this.XMaxText = new System.Windows.Forms.TextBox();
            this.XMinText = new System.Windows.Forms.TextBox();
            this.StepLabel = new System.Windows.Forms.Label();
            this.XMaxLabel = new System.Windows.Forms.Label();
            this.XMinLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).BeginInit();
            this.UniformDistGroup.SuspendLayout();
            this.ErlangDistGroup.SuspendLayout();
            this.CommonOptionsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // Chart
            // 
            chartArea1.Name = "DensityArea";
            chartArea2.Name = "DistributionArea";
            this.Chart.ChartAreas.Add(chartArea1);
            this.Chart.ChartAreas.Add(chartArea2);
            legend1.Alignment = System.Drawing.StringAlignment.Far;
            legend1.DockedToChartArea = "DensityArea";
            legend1.Name = "DensityLegend";
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.DockedToChartArea = "DistributionArea";
            legend2.Name = "DistributionLegend";
            this.Chart.Legends.Add(legend1);
            this.Chart.Legends.Add(legend2);
            this.Chart.Location = new System.Drawing.Point(12, 12);
            this.Chart.Name = "Chart";
            series1.BorderWidth = 4;
            series1.ChartArea = "DensityArea";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "DensityLegend";
            series1.LegendText = "Равномерное";
            series1.Name = "UniDensity";
            series2.BorderWidth = 2;
            series2.ChartArea = "DensityArea";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "DensityLegend";
            series2.LegendText = "Эрланга";
            series2.Name = "ErlangDensity";
            series3.BorderWidth = 4;
            series3.ChartArea = "DistributionArea";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "DistributionLegend";
            series3.LegendText = "Равномерное";
            series3.Name = "UniDistribution";
            series4.BorderWidth = 2;
            series4.ChartArea = "DistributionArea";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "DistributionLegend";
            series4.LegendText = "Эрланга";
            series4.Name = "ErlangDistribution";
            this.Chart.Series.Add(series1);
            this.Chart.Series.Add(series2);
            this.Chart.Series.Add(series3);
            this.Chart.Series.Add(series4);
            this.Chart.Size = new System.Drawing.Size(616, 537);
            this.Chart.TabIndex = 0;
            this.Chart.Text = "График";
            title1.DockedToChartArea = "DensityArea";
            title1.IsDockedInsideChartArea = false;
            title1.Name = "DensityTitle";
            title1.Text = "Плотность распределения";
            title2.DockedToChartArea = "DistributionArea";
            title2.IsDockedInsideChartArea = false;
            title2.Name = "DistributionTitle";
            title2.Text = "Функция распределения";
            this.Chart.Titles.Add(title1);
            this.Chart.Titles.Add(title2);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(634, 521);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(138, 28);
            this.StartButton.TabIndex = 1;
            this.StartButton.Text = "Вычислить";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // UniformDistGroup
            // 
            this.UniformDistGroup.Controls.Add(this.BText);
            this.UniformDistGroup.Controls.Add(this.AText);
            this.UniformDistGroup.Controls.Add(this.BLabel);
            this.UniformDistGroup.Controls.Add(this.ALabel);
            this.UniformDistGroup.Location = new System.Drawing.Point(634, 12);
            this.UniformDistGroup.Name = "UniformDistGroup";
            this.UniformDistGroup.Size = new System.Drawing.Size(138, 88);
            this.UniformDistGroup.TabIndex = 2;
            this.UniformDistGroup.TabStop = false;
            this.UniformDistGroup.Text = "Равномерное распределение";
            // 
            // BText
            // 
            this.BText.Location = new System.Drawing.Point(32, 60);
            this.BText.Name = "BText";
            this.BText.Size = new System.Drawing.Size(100, 20);
            this.BText.TabIndex = 5;
            this.BText.Text = "4";
            this.BText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BText_KeyPress);
            // 
            // AText
            // 
            this.AText.Location = new System.Drawing.Point(32, 34);
            this.AText.Name = "AText";
            this.AText.Size = new System.Drawing.Size(100, 20);
            this.AText.TabIndex = 4;
            this.AText.Text = "1";
            this.AText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AText_KeyPress);
            // 
            // BLabel
            // 
            this.BLabel.AutoSize = true;
            this.BLabel.Location = new System.Drawing.Point(10, 63);
            this.BLabel.Name = "BLabel";
            this.BLabel.Size = new System.Drawing.Size(13, 13);
            this.BLabel.TabIndex = 3;
            this.BLabel.Text = "b";
            // 
            // ALabel
            // 
            this.ALabel.AutoSize = true;
            this.ALabel.Location = new System.Drawing.Point(10, 37);
            this.ALabel.Name = "ALabel";
            this.ALabel.Size = new System.Drawing.Size(13, 13);
            this.ALabel.TabIndex = 2;
            this.ALabel.Text = "a";
            // 
            // ErlangDistGroup
            // 
            this.ErlangDistGroup.Controls.Add(this.SegmentsText);
            this.ErlangDistGroup.Controls.Add(this.SegmentsLabel);
            this.ErlangDistGroup.Controls.Add(this.GammaText);
            this.ErlangDistGroup.Controls.Add(this.GammaLabel);
            this.ErlangDistGroup.Controls.Add(this.LambdaText);
            this.ErlangDistGroup.Controls.Add(this.LambdaLabel);
            this.ErlangDistGroup.Location = new System.Drawing.Point(634, 106);
            this.ErlangDistGroup.Name = "ErlangDistGroup";
            this.ErlangDistGroup.Size = new System.Drawing.Size(138, 116);
            this.ErlangDistGroup.TabIndex = 3;
            this.ErlangDistGroup.TabStop = false;
            this.ErlangDistGroup.Text = "Распределение Эрланга";
            // 
            // SegmentsText
            // 
            this.SegmentsText.Location = new System.Drawing.Point(32, 87);
            this.SegmentsText.Name = "SegmentsText";
            this.SegmentsText.Size = new System.Drawing.Size(100, 20);
            this.SegmentsText.TabIndex = 5;
            this.SegmentsText.Text = "1000";
            this.SegmentsText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SegmentsText_KeyPress);
            // 
            // SegmentsLabel
            // 
            this.SegmentsLabel.AutoSize = true;
            this.SegmentsLabel.Location = new System.Drawing.Point(10, 90);
            this.SegmentsLabel.Name = "SegmentsLabel";
            this.SegmentsLabel.Size = new System.Drawing.Size(15, 13);
            this.SegmentsLabel.TabIndex = 4;
            this.SegmentsLabel.Text = "N";
            // 
            // GammaText
            // 
            this.GammaText.Location = new System.Drawing.Point(32, 61);
            this.GammaText.Name = "GammaText";
            this.GammaText.Size = new System.Drawing.Size(100, 20);
            this.GammaText.TabIndex = 3;
            this.GammaText.Text = "1";
            this.GammaText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GammaText_KeyPress);
            // 
            // GammaLabel
            // 
            this.GammaLabel.AutoSize = true;
            this.GammaLabel.Location = new System.Drawing.Point(10, 64);
            this.GammaLabel.Name = "GammaLabel";
            this.GammaLabel.Size = new System.Drawing.Size(13, 13);
            this.GammaLabel.TabIndex = 2;
            this.GammaLabel.Text = "γ";
            // 
            // LambdaText
            // 
            this.LambdaText.Location = new System.Drawing.Point(32, 34);
            this.LambdaText.Name = "LambdaText";
            this.LambdaText.Size = new System.Drawing.Size(100, 20);
            this.LambdaText.TabIndex = 1;
            this.LambdaText.Text = "1";
            this.LambdaText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LambdaText_KeyPress);
            // 
            // LambdaLabel
            // 
            this.LambdaLabel.AutoSize = true;
            this.LambdaLabel.Location = new System.Drawing.Point(10, 37);
            this.LambdaLabel.Name = "LambdaLabel";
            this.LambdaLabel.Size = new System.Drawing.Size(12, 13);
            this.LambdaLabel.TabIndex = 0;
            this.LambdaLabel.Text = "λ";
            // 
            // CommonOptionsGroup
            // 
            this.CommonOptionsGroup.Controls.Add(this.StepText);
            this.CommonOptionsGroup.Controls.Add(this.XMaxText);
            this.CommonOptionsGroup.Controls.Add(this.XMinText);
            this.CommonOptionsGroup.Controls.Add(this.StepLabel);
            this.CommonOptionsGroup.Controls.Add(this.XMaxLabel);
            this.CommonOptionsGroup.Controls.Add(this.XMinLabel);
            this.CommonOptionsGroup.Location = new System.Drawing.Point(634, 228);
            this.CommonOptionsGroup.Name = "CommonOptionsGroup";
            this.CommonOptionsGroup.Size = new System.Drawing.Size(138, 100);
            this.CommonOptionsGroup.TabIndex = 4;
            this.CommonOptionsGroup.TabStop = false;
            this.CommonOptionsGroup.Text = "Общие настройки";
            // 
            // StepText
            // 
            this.StepText.Location = new System.Drawing.Point(48, 71);
            this.StepText.Name = "StepText";
            this.StepText.Size = new System.Drawing.Size(84, 20);
            this.StepText.TabIndex = 5;
            this.StepText.Text = "0,001";
            this.StepText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StepText_KeyPress);
            // 
            // XMaxText
            // 
            this.XMaxText.Location = new System.Drawing.Point(48, 45);
            this.XMaxText.Name = "XMaxText";
            this.XMaxText.Size = new System.Drawing.Size(84, 20);
            this.XMaxText.TabIndex = 4;
            this.XMaxText.Text = "6";
            this.XMaxText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XMaxText_KeyPress);
            // 
            // XMinText
            // 
            this.XMinText.Location = new System.Drawing.Point(48, 19);
            this.XMinText.Name = "XMinText";
            this.XMinText.Size = new System.Drawing.Size(84, 20);
            this.XMinText.TabIndex = 3;
            this.XMinText.Text = "0";
            this.XMinText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XMinText_KeyPress);
            // 
            // StepLabel
            // 
            this.StepLabel.AutoSize = true;
            this.StepLabel.Location = new System.Drawing.Point(10, 74);
            this.StepLabel.Name = "StepLabel";
            this.StepLabel.Size = new System.Drawing.Size(27, 13);
            this.StepLabel.TabIndex = 2;
            this.StepLabel.Text = "Шаг";
            // 
            // XMaxLabel
            // 
            this.XMaxLabel.AutoSize = true;
            this.XMaxLabel.Location = new System.Drawing.Point(10, 48);
            this.XMaxLabel.Name = "XMaxLabel";
            this.XMaxLabel.Size = new System.Drawing.Size(34, 13);
            this.XMaxLabel.TabIndex = 1;
            this.XMaxLabel.Text = "x max";
            // 
            // XMinLabel
            // 
            this.XMinLabel.AutoSize = true;
            this.XMinLabel.Location = new System.Drawing.Point(10, 22);
            this.XMinLabel.Name = "XMinLabel";
            this.XMinLabel.Size = new System.Drawing.Size(31, 13);
            this.XMinLabel.TabIndex = 0;
            this.XMinLabel.Text = "x min";
            // 
            // MainForm
            // 
            this.AcceptButton = this.StartButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.CommonOptionsGroup);
            this.Controls.Add(this.ErlangDistGroup);
            this.Controls.Add(this.UniformDistGroup);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.Chart);
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Графики распределения случайных величин";
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).EndInit();
            this.UniformDistGroup.ResumeLayout(false);
            this.UniformDistGroup.PerformLayout();
            this.ErlangDistGroup.ResumeLayout(false);
            this.ErlangDistGroup.PerformLayout();
            this.CommonOptionsGroup.ResumeLayout(false);
            this.CommonOptionsGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart Chart;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.GroupBox UniformDistGroup;
        private System.Windows.Forms.GroupBox ErlangDistGroup;
        private System.Windows.Forms.Label BLabel;
        private System.Windows.Forms.Label ALabel;
        private System.Windows.Forms.Label LambdaLabel;
        private System.Windows.Forms.GroupBox CommonOptionsGroup;
        private System.Windows.Forms.Label StepLabel;
        private System.Windows.Forms.Label XMaxLabel;
        private System.Windows.Forms.Label XMinLabel;
        private System.Windows.Forms.TextBox BText;
        private System.Windows.Forms.TextBox AText;
        private System.Windows.Forms.TextBox LambdaText;
        private System.Windows.Forms.TextBox XMaxText;
        private System.Windows.Forms.TextBox XMinText;
        private System.Windows.Forms.TextBox StepText;
        private System.Windows.Forms.TextBox GammaText;
        private System.Windows.Forms.Label GammaLabel;
        private System.Windows.Forms.Label SegmentsLabel;
        private System.Windows.Forms.TextBox SegmentsText;
    }
}

