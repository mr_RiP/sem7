﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer.EventGenerators
{
    public abstract class EventGenerator
    {
        private static Random random = new Random();

        private static object lockObject = new object();

        public abstract double Average { get; }

        public abstract double NextEvent(double currTime);

        public static double Next()
        {
            lock (lockObject)
                return random.NextDouble();
        }

        public static double Next(double min, double max)
        {
            double diff = max - min;
            if (diff < 0 || diff.Compare(0))
                throw new ArgumentOutOfRangeException();

            return Next() * diff + min;
        }

        public static double[] NextSequence(int count)
        {
            if (count <= 0)
                throw new ArgumentOutOfRangeException("count");

            var res = new double[count];
            lock (lockObject)
                for (int i = 0; i < res.Length; ++i)
                    res[i] = random.NextDouble();
            return res;
        }

        public static double[] NextSequence(double min, double max, int count)
        {
            if (count <= 0)
                throw new ArgumentOutOfRangeException("count");

            double diff = max - min;
            if (diff < 0 || diff.Compare(0))
                throw new ArgumentOutOfRangeException();

            var res = NextSequence(count);
            for (int i = 0; i < res.Length; ++i)
                res[i] = res[i] * diff + min;
            return res;
        }
    }
}
