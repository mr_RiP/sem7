﻿using QueueAnalyzer;
using QueueAnalyzer.EventGenerators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class Lab5 : Form, IUiWorker
    {
        public Lab5()
        {
            InitializeComponent();
        }

        private TextBox[] eventSources = new TextBox[12];

        private IList<EventGenerator> generators;

        private double interval;

        private int maxSize;

        private long step = 1;

        private int stopAfter;

        private QueueController controller = null;

        long IUiWorker.DisplayInterval
        {
            get
            {
                return step;
            }
        }

        private async void btnSetEvents_Click(object sender, EventArgs e)
        {
            GetGenerators();
            if (controller != null)
                await Task.Run(() => controller.Generators = generators);
        }

        private void lab5_Load(object sender, EventArgs e)
        {
            eventSources[0] = tb01;
            eventSources[1] = tb02;

            eventSources[2] = tb10;
            eventSources[3] = tb11;

            eventSources[4] = tb20;
            eventSources[5] = tb21;

            eventSources[6] = tb30;
            eventSources[7] = tb31;

            eventSources[8] = tb40;
            eventSources[9] = tb41;

            eventSources[10] = tb50;
            eventSources[11] = tb51;
        }

        private void GetGenerators()
        {
            var nums = eventSources.Select(x => double.Parse(x.Text, CultureInfo.InvariantCulture)).ToList();
            var res = new List<EventGenerator>(eventSources.Length / 2);
            for (int i = 0; i < nums.Count; i += 2)
                res.Add(new UniformEventGenerator(nums[i], nums[i + 1]));
            generators = res;
        }

        private void GetParams()
        {
            interval = double.Parse(tbInterval.Text, CultureInfo.InvariantCulture);
            step = long.Parse(tbStep.Text, CultureInfo.InvariantCulture);
            maxSize = int.Parse(tbMaxSize.Text, CultureInfo.InvariantCulture);
            stopAfter = int.Parse(tbStopAfter.Text, CultureInfo.InvariantCulture);
        }

        void IUiWorker.Display(QueueStats stats)
        {
            Invoke((MethodInvoker)(() => DisplayInner(stats)));
        }

        Task IUiWorker.DisplayAsync(QueueStats stats)
        {
            return Task.Factory.FromAsync(BeginInvoke((MethodInvoker)(() => DisplayInner(stats))), (result) => { });
        }

        private void DisplayInner(QueueStats stats)
        {
            lbCurrTime.Text = stats.CurrTime.ToString();
            lbDropped.Text = stats.Dropped.ToString();
            lbPassed.Text = stats.Passed.ToString();

            if (stats.Dropped != 0 && stats.Passed != 0)
                lbPart.Text = ((double)stats.Dropped / (stats.Dropped + stats.Passed)).ToString();
            if (stats.Passed >= stopAfter)
                btnStop_Click(this, null);
        }

        private void ClearLabels()
        {
            lbCurrTime.Text = "";
            lbDropped.Text = "";
            lbPassed.Text = "";
            lbPart.Text = "";
        }

        private async void btnQueueParams_Click(object sender, EventArgs e)
        {
            GetParams();
            if (controller != null)
                await Task.Run(() =>
                {
                    controller.Interval = interval;
                    controller.MaxSize = maxSize;
                });
        }

        private async void btnStart_Click(object sender, EventArgs e)
        {
            if (controller == null)
            {
                GetGenerators();
                GetParams();
                controller = new QueueController(generators, new QueueParams(maxSize, interval), this);
            }
            btnStart.Enabled = false;
            await Task.Run(() => controller.Start());
        }

        private async void btnStop_Click(object sender, EventArgs e)
        {
            await Task.Run(() => controller?.Stop());
            btnStart.Enabled = true;
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (controller != null)
            {
                await Task.Run(() => controller.Dispose());
                controller = null;
                ClearLabels();
                btnStart.Enabled = true;
            }
        }
    }
}
