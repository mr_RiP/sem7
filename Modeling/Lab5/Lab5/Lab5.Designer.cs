﻿namespace Lab5
{
    partial class Lab5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb01 = new System.Windows.Forms.TextBox();
            this.tb02 = new System.Windows.Forms.TextBox();
            this.tb11 = new System.Windows.Forms.TextBox();
            this.tb10 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.tb20 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb31 = new System.Windows.Forms.TextBox();
            this.tb30 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tb41 = new System.Windows.Forms.TextBox();
            this.tb40 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tb51 = new System.Windows.Forms.TextBox();
            this.tb50 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnSetEvents = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.tbMaxSize = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.tbStep = new System.Windows.Forms.TextBox();
            this.btnQueueParams = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbPassed = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbStopAfter = new System.Windows.Forms.TextBox();
            this.lbDropped = new System.Windows.Forms.Label();
            this.lbCurrTime = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbPart = new System.Windows.Forms.Label();
            this.gbOperators = new System.Windows.Forms.GroupBox();
            this.gbThirdOperator = new System.Windows.Forms.GroupBox();
            this.gbSecondOperator = new System.Windows.Forms.GroupBox();
            this.gbFirstOperator = new System.Windows.Forms.GroupBox();
            this.gbComputers = new System.Windows.Forms.GroupBox();
            this.gbSecondComputer = new System.Windows.Forms.GroupBox();
            this.gbFirstComputer = new System.Windows.Forms.GroupBox();
            this.gbEventGeneration = new System.Windows.Forms.GroupBox();
            this.dbModel = new System.Windows.Forms.GroupBox();
            this.gbSimulation = new System.Windows.Forms.GroupBox();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.gbOperators.SuspendLayout();
            this.gbThirdOperator.SuspendLayout();
            this.gbSecondOperator.SuspendLayout();
            this.gbFirstOperator.SuspendLayout();
            this.gbComputers.SuspendLayout();
            this.gbSecondComputer.SuspendLayout();
            this.gbFirstComputer.SuspendLayout();
            this.gbEventGeneration.SuspendLayout();
            this.dbModel.SuspendLayout();
            this.gbSimulation.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "От:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "До:";
            // 
            // tb01
            // 
            this.tb01.Location = new System.Drawing.Point(42, 19);
            this.tb01.Name = "tb01";
            this.tb01.Size = new System.Drawing.Size(100, 20);
            this.tb01.TabIndex = 8;
            this.tb01.Text = "8";
            // 
            // tb02
            // 
            this.tb02.Location = new System.Drawing.Point(42, 45);
            this.tb02.Name = "tb02";
            this.tb02.Size = new System.Drawing.Size(100, 20);
            this.tb02.TabIndex = 9;
            this.tb02.Text = "12";
            // 
            // tb11
            // 
            this.tb11.Location = new System.Drawing.Point(36, 44);
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(100, 20);
            this.tb11.TabIndex = 13;
            this.tb11.Text = "25";
            // 
            // tb10
            // 
            this.tb10.Location = new System.Drawing.Point(36, 16);
            this.tb10.Name = "tb10";
            this.tb10.Size = new System.Drawing.Size(100, 20);
            this.tb10.TabIndex = 12;
            this.tb10.Text = "15";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "До:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "От:";
            // 
            // tb21
            // 
            this.tb21.Location = new System.Drawing.Point(36, 44);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(100, 20);
            this.tb21.TabIndex = 17;
            this.tb21.Text = "50";
            // 
            // tb20
            // 
            this.tb20.Location = new System.Drawing.Point(36, 16);
            this.tb20.Name = "tb20";
            this.tb20.Size = new System.Drawing.Size(100, 20);
            this.tb20.TabIndex = 16;
            this.tb20.Text = "30";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "До:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "От:";
            // 
            // tb31
            // 
            this.tb31.Location = new System.Drawing.Point(36, 45);
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(100, 20);
            this.tb31.TabIndex = 21;
            this.tb31.Text = "60";
            // 
            // tb30
            // 
            this.tb30.Location = new System.Drawing.Point(36, 19);
            this.tb30.Name = "tb30";
            this.tb30.Size = new System.Drawing.Size(100, 20);
            this.tb30.TabIndex = 20;
            this.tb30.Text = "20";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "До:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "От:";
            // 
            // tb41
            // 
            this.tb41.Location = new System.Drawing.Point(35, 45);
            this.tb41.Name = "tb41";
            this.tb41.Size = new System.Drawing.Size(100, 20);
            this.tb41.TabIndex = 25;
            this.tb41.Text = "15";
            // 
            // tb40
            // 
            this.tb40.Location = new System.Drawing.Point(35, 19);
            this.tb40.Name = "tb40";
            this.tb40.Size = new System.Drawing.Size(100, 20);
            this.tb40.TabIndex = 24;
            this.tb40.Text = "15";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "До:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "От:";
            // 
            // tb51
            // 
            this.tb51.Location = new System.Drawing.Point(35, 45);
            this.tb51.Name = "tb51";
            this.tb51.Size = new System.Drawing.Size(100, 20);
            this.tb51.TabIndex = 29;
            this.tb51.Text = "30";
            // 
            // tb50
            // 
            this.tb50.Location = new System.Drawing.Point(35, 19);
            this.tb50.Name = "tb50";
            this.tb50.Size = new System.Drawing.Size(100, 20);
            this.tb50.TabIndex = 28;
            this.tb50.Text = "30";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(25, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "До:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "От:";
            // 
            // btnSetEvents
            // 
            this.btnSetEvents.Location = new System.Drawing.Point(6, 554);
            this.btnSetEvents.Name = "btnSetEvents";
            this.btnSetEvents.Size = new System.Drawing.Size(159, 23);
            this.btnSetEvents.TabIndex = 30;
            this.btnSetEvents.Text = "Задать";
            this.btnSetEvents.UseVisualStyleBackColor = true;
            this.btnSetEvents.Click += new System.EventHandler(this.btnSetEvents_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(174, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Максимальный размер очереди:";
            // 
            // tbMaxSize
            // 
            this.tbMaxSize.Location = new System.Drawing.Point(186, 19);
            this.tbMaxSize.Name = "tbMaxSize";
            this.tbMaxSize.Size = new System.Drawing.Size(100, 20);
            this.tbMaxSize.TabIndex = 32;
            this.tbMaxSize.Text = "300";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(142, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Интервал моделирования:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 76);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Шаг отображения:";
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(186, 46);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(100, 20);
            this.tbInterval.TabIndex = 35;
            this.tbInterval.Text = "0.01";
            // 
            // tbStep
            // 
            this.tbStep.Location = new System.Drawing.Point(186, 73);
            this.tbStep.Name = "tbStep";
            this.tbStep.Size = new System.Drawing.Size(100, 20);
            this.tbStep.TabIndex = 36;
            this.tbStep.Text = "1";
            // 
            // btnQueueParams
            // 
            this.btnQueueParams.Location = new System.Drawing.Point(12, 126);
            this.btnQueueParams.Name = "btnQueueParams";
            this.btnQueueParams.Size = new System.Drawing.Size(274, 23);
            this.btnQueueParams.TabIndex = 37;
            this.btnQueueParams.Text = "Задать";
            this.btnQueueParams.UseVisualStyleBackColor = true;
            this.btnQueueParams.Click += new System.EventHandler(this.btnQueueParams_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(232, 501);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(217, 23);
            this.btnStart.TabIndex = 38;
            this.btnStart.Text = "Начать";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(232, 530);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(217, 23);
            this.btnStop.TabIndex = 39;
            this.btnStop.Text = "Остановить";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(232, 559);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(217, 23);
            this.btnDelete.TabIndex = 40;
            this.btnDelete.Text = "Очистить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(109, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Заявок обработано:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 50);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "Заявок пропущено:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 44;
            this.label25.Text = "Времени прошло:";
            // 
            // lbPassed
            // 
            this.lbPassed.AutoSize = true;
            this.lbPassed.Location = new System.Drawing.Point(125, 27);
            this.lbPassed.Name = "lbPassed";
            this.lbPassed.Size = new System.Drawing.Size(0, 13);
            this.lbPassed.TabIndex = 45;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 103);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 13);
            this.label26.TabIndex = 46;
            this.label26.Text = "Остановка после:";
            // 
            // tbStopAfter
            // 
            this.tbStopAfter.Location = new System.Drawing.Point(186, 100);
            this.tbStopAfter.Name = "tbStopAfter";
            this.tbStopAfter.Size = new System.Drawing.Size(100, 20);
            this.tbStopAfter.TabIndex = 47;
            this.tbStopAfter.Text = "300";
            // 
            // lbDropped
            // 
            this.lbDropped.AutoSize = true;
            this.lbDropped.Location = new System.Drawing.Point(122, 50);
            this.lbDropped.Name = "lbDropped";
            this.lbDropped.Size = new System.Drawing.Size(0, 13);
            this.lbDropped.TabIndex = 48;
            // 
            // lbCurrTime
            // 
            this.lbCurrTime.AutoSize = true;
            this.lbCurrTime.Location = new System.Drawing.Point(112, 74);
            this.lbCurrTime.Name = "lbCurrTime";
            this.lbCurrTime.Size = new System.Drawing.Size(0, 13);
            this.lbCurrTime.TabIndex = 49;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 97);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(148, 13);
            this.label27.TabIndex = 50;
            this.label27.Text = "Доля пропущенных заявок:";
            // 
            // lbPart
            // 
            this.lbPart.AutoSize = true;
            this.lbPart.Location = new System.Drawing.Point(167, 97);
            this.lbPart.Name = "lbPart";
            this.lbPart.Size = new System.Drawing.Size(0, 13);
            this.lbPart.TabIndex = 51;
            // 
            // gbOperators
            // 
            this.gbOperators.Controls.Add(this.gbThirdOperator);
            this.gbOperators.Controls.Add(this.gbSecondOperator);
            this.gbOperators.Controls.Add(this.gbFirstOperator);
            this.gbOperators.Location = new System.Drawing.Point(6, 98);
            this.gbOperators.Name = "gbOperators";
            this.gbOperators.Size = new System.Drawing.Size(159, 263);
            this.gbOperators.TabIndex = 52;
            this.gbOperators.TabStop = false;
            this.gbOperators.Text = "Операторы";
            // 
            // gbThirdOperator
            // 
            this.gbThirdOperator.Controls.Add(this.label14);
            this.gbThirdOperator.Controls.Add(this.tb31);
            this.gbThirdOperator.Controls.Add(this.tb30);
            this.gbThirdOperator.Controls.Add(this.label13);
            this.gbThirdOperator.Location = new System.Drawing.Point(6, 177);
            this.gbThirdOperator.Name = "gbThirdOperator";
            this.gbThirdOperator.Size = new System.Drawing.Size(145, 75);
            this.gbThirdOperator.TabIndex = 24;
            this.gbThirdOperator.TabStop = false;
            this.gbThirdOperator.Text = "Третий";
            // 
            // gbSecondOperator
            // 
            this.gbSecondOperator.Controls.Add(this.label12);
            this.gbSecondOperator.Controls.Add(this.tb21);
            this.gbSecondOperator.Controls.Add(this.tb20);
            this.gbSecondOperator.Controls.Add(this.label11);
            this.gbSecondOperator.Location = new System.Drawing.Point(6, 98);
            this.gbSecondOperator.Name = "gbSecondOperator";
            this.gbSecondOperator.Size = new System.Drawing.Size(145, 73);
            this.gbSecondOperator.TabIndex = 23;
            this.gbSecondOperator.TabStop = false;
            this.gbSecondOperator.Text = "Второй";
            // 
            // gbFirstOperator
            // 
            this.gbFirstOperator.Controls.Add(this.label10);
            this.gbFirstOperator.Controls.Add(this.tb11);
            this.gbFirstOperator.Controls.Add(this.tb10);
            this.gbFirstOperator.Controls.Add(this.label9);
            this.gbFirstOperator.Location = new System.Drawing.Point(6, 19);
            this.gbFirstOperator.Name = "gbFirstOperator";
            this.gbFirstOperator.Size = new System.Drawing.Size(145, 73);
            this.gbFirstOperator.TabIndex = 22;
            this.gbFirstOperator.TabStop = false;
            this.gbFirstOperator.Text = "Первый";
            // 
            // gbComputers
            // 
            this.gbComputers.Controls.Add(this.gbSecondComputer);
            this.gbComputers.Controls.Add(this.gbFirstComputer);
            this.gbComputers.Location = new System.Drawing.Point(6, 367);
            this.gbComputers.Name = "gbComputers";
            this.gbComputers.Size = new System.Drawing.Size(159, 181);
            this.gbComputers.TabIndex = 53;
            this.gbComputers.TabStop = false;
            this.gbComputers.Text = "Компьютеры";
            // 
            // gbSecondComputer
            // 
            this.gbSecondComputer.Controls.Add(this.label18);
            this.gbSecondComputer.Controls.Add(this.label17);
            this.gbSecondComputer.Controls.Add(this.tb50);
            this.gbSecondComputer.Controls.Add(this.tb51);
            this.gbSecondComputer.Location = new System.Drawing.Point(6, 100);
            this.gbSecondComputer.Name = "gbSecondComputer";
            this.gbSecondComputer.Size = new System.Drawing.Size(145, 73);
            this.gbSecondComputer.TabIndex = 1;
            this.gbSecondComputer.TabStop = false;
            this.gbSecondComputer.Text = "Второй";
            // 
            // gbFirstComputer
            // 
            this.gbFirstComputer.Controls.Add(this.tb41);
            this.gbFirstComputer.Controls.Add(this.tb40);
            this.gbFirstComputer.Controls.Add(this.label15);
            this.gbFirstComputer.Controls.Add(this.label16);
            this.gbFirstComputer.Location = new System.Drawing.Point(7, 19);
            this.gbFirstComputer.Name = "gbFirstComputer";
            this.gbFirstComputer.Size = new System.Drawing.Size(144, 75);
            this.gbFirstComputer.TabIndex = 0;
            this.gbFirstComputer.TabStop = false;
            this.gbFirstComputer.Text = "Первый";
            // 
            // gbEventGeneration
            // 
            this.gbEventGeneration.Controls.Add(this.tb01);
            this.gbEventGeneration.Controls.Add(this.tb02);
            this.gbEventGeneration.Controls.Add(this.label7);
            this.gbEventGeneration.Controls.Add(this.label8);
            this.gbEventGeneration.Location = new System.Drawing.Point(6, 19);
            this.gbEventGeneration.Name = "gbEventGeneration";
            this.gbEventGeneration.Size = new System.Drawing.Size(159, 73);
            this.gbEventGeneration.TabIndex = 54;
            this.gbEventGeneration.TabStop = false;
            this.gbEventGeneration.Text = "Частота событий";
            // 
            // dbModel
            // 
            this.dbModel.Controls.Add(this.gbEventGeneration);
            this.dbModel.Controls.Add(this.btnSetEvents);
            this.dbModel.Controls.Add(this.gbComputers);
            this.dbModel.Controls.Add(this.gbOperators);
            this.dbModel.Location = new System.Drawing.Point(12, 12);
            this.dbModel.Name = "dbModel";
            this.dbModel.Size = new System.Drawing.Size(171, 585);
            this.dbModel.TabIndex = 55;
            this.dbModel.TabStop = false;
            this.dbModel.Text = "Настройки модели";
            // 
            // gbSimulation
            // 
            this.gbSimulation.Controls.Add(this.label19);
            this.gbSimulation.Controls.Add(this.tbMaxSize);
            this.gbSimulation.Controls.Add(this.label20);
            this.gbSimulation.Controls.Add(this.label21);
            this.gbSimulation.Controls.Add(this.tbInterval);
            this.gbSimulation.Controls.Add(this.tbStep);
            this.gbSimulation.Controls.Add(this.tbStopAfter);
            this.gbSimulation.Controls.Add(this.btnQueueParams);
            this.gbSimulation.Controls.Add(this.label26);
            this.gbSimulation.Location = new System.Drawing.Point(189, 12);
            this.gbSimulation.Name = "gbSimulation";
            this.gbSimulation.Size = new System.Drawing.Size(296, 162);
            this.gbSimulation.TabIndex = 56;
            this.gbSimulation.TabStop = false;
            this.gbSimulation.Text = "Настройки симуляции";
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.label23);
            this.gbResults.Controls.Add(this.label24);
            this.gbResults.Controls.Add(this.label25);
            this.gbResults.Controls.Add(this.lbPart);
            this.gbResults.Controls.Add(this.lbPassed);
            this.gbResults.Controls.Add(this.label27);
            this.gbResults.Controls.Add(this.lbDropped);
            this.gbResults.Controls.Add(this.lbCurrTime);
            this.gbResults.Location = new System.Drawing.Point(189, 181);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(296, 122);
            this.gbResults.TabIndex = 57;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Результаты";
            // 
            // Lab5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 606);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbSimulation);
            this.Controls.Add(this.dbModel);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "Lab5";
            this.Text = "Lab 5";
            this.Load += new System.EventHandler(this.lab5_Load);
            this.gbOperators.ResumeLayout(false);
            this.gbThirdOperator.ResumeLayout(false);
            this.gbThirdOperator.PerformLayout();
            this.gbSecondOperator.ResumeLayout(false);
            this.gbSecondOperator.PerformLayout();
            this.gbFirstOperator.ResumeLayout(false);
            this.gbFirstOperator.PerformLayout();
            this.gbComputers.ResumeLayout(false);
            this.gbSecondComputer.ResumeLayout(false);
            this.gbSecondComputer.PerformLayout();
            this.gbFirstComputer.ResumeLayout(false);
            this.gbFirstComputer.PerformLayout();
            this.gbEventGeneration.ResumeLayout(false);
            this.gbEventGeneration.PerformLayout();
            this.dbModel.ResumeLayout(false);
            this.gbSimulation.ResumeLayout(false);
            this.gbSimulation.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.gbResults.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb01;
        private System.Windows.Forms.TextBox tb02;
        private System.Windows.Forms.TextBox tb11;
        private System.Windows.Forms.TextBox tb10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.TextBox tb20;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb31;
        private System.Windows.Forms.TextBox tb30;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb41;
        private System.Windows.Forms.TextBox tb40;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb51;
        private System.Windows.Forms.TextBox tb50;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnSetEvents;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbMaxSize;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.TextBox tbStep;
        private System.Windows.Forms.Button btnQueueParams;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbPassed;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbStopAfter;
        private System.Windows.Forms.Label lbDropped;
        private System.Windows.Forms.Label lbCurrTime;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lbPart;
        private System.Windows.Forms.GroupBox gbOperators;
        private System.Windows.Forms.GroupBox gbThirdOperator;
        private System.Windows.Forms.GroupBox gbSecondOperator;
        private System.Windows.Forms.GroupBox gbFirstOperator;
        private System.Windows.Forms.GroupBox gbComputers;
        private System.Windows.Forms.GroupBox gbSecondComputer;
        private System.Windows.Forms.GroupBox gbFirstComputer;
        private System.Windows.Forms.GroupBox gbEventGeneration;
        private System.Windows.Forms.GroupBox dbModel;
        private System.Windows.Forms.GroupBox gbSimulation;
        private System.Windows.Forms.GroupBox gbResults;
    }
}

