﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common
{
    public static class GridExtensions
    {
        public static void Clear(this DataGridView g)
        {
            for (int i = 0; i < g.ColumnCount; ++i)
                for (int j = 0; j < g.RowCount; ++j)
                    g[i, j].Value = "";
        }

        public static void ClearColumn(this DataGridView g, int ind)
        {
            if (ind < 0 || ind >= g.ColumnCount)
                throw new ArgumentOutOfRangeException("ind");

            for (int i = 0; i < g.RowCount; ++i)
                g[ind, i].Value = "";
        }

        public static void ClearRow(this DataGridView g, int ind)
        {
            if (ind < 0 || ind >= g.RowCount)
                throw new ArgumentOutOfRangeException("ind");

            for (int i = 0; i < g.ColumnCount; ++i)
                g[i, ind].Value = "";
        }

        public static void ReqiureRows(this DataGridView g, int rows)
        {
            if (rows < 1)
                throw new ArgumentOutOfRangeException("rows");

            if (g.RowCount < rows)
                g.RowCount = rows;
        }

        public static void DisableSorting(this DataGridView g)
        {
            foreach (DataGridViewColumn column in g.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }
    }
}
