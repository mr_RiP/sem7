﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class FloatingComparator
    {
        private const float epsFloat = 1e-3f;

        private const double epsDouble = 1e-7;

        public static bool Compare(this float a, float b)
        {
            return Math.Abs(a - b) < epsFloat;
        }

        public static bool Compare(this double a, double b)
        {
            return Math.Abs(a - b) < epsDouble;
        }
    }
}
