﻿namespace lab5
{
    partial class lab5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb01 = new System.Windows.Forms.TextBox();
            this.tb02 = new System.Windows.Forms.TextBox();
            this.tb11 = new System.Windows.Forms.TextBox();
            this.tb10 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.tb20 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb31 = new System.Windows.Forms.TextBox();
            this.tb30 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tb41 = new System.Windows.Forms.TextBox();
            this.tb40 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tb51 = new System.Windows.Forms.TextBox();
            this.tb50 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnSetEvents = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.tbMaxSize = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.tbStep = new System.Windows.Forms.TextBox();
            this.btnQueueParams = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbPassed = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbStopAfter = new System.Windows.Forms.TextBox();
            this.lbDropped = new System.Windows.Forms.Label();
            this.lbCurrTime = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbPart = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Частота событий:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Обработка первым оператором:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Обработка вторым оператором:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Обработка третьим оператором";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Обработка в первой очереди:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 356);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Обработка во второй очереди:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "От:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "До:";
            // 
            // tb01
            // 
            this.tb01.Location = new System.Drawing.Point(52, 26);
            this.tb01.Name = "tb01";
            this.tb01.Size = new System.Drawing.Size(100, 20);
            this.tb01.TabIndex = 8;
            this.tb01.Text = "8";
            // 
            // tb02
            // 
            this.tb02.Location = new System.Drawing.Point(54, 44);
            this.tb02.Name = "tb02";
            this.tb02.Size = new System.Drawing.Size(100, 20);
            this.tb02.TabIndex = 9;
            this.tb02.Text = "12";
            // 
            // tb11
            // 
            this.tb11.Location = new System.Drawing.Point(54, 108);
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(100, 20);
            this.tb11.TabIndex = 13;
            this.tb11.Text = "25";
            // 
            // tb10
            // 
            this.tb10.Location = new System.Drawing.Point(52, 90);
            this.tb10.Name = "tb10";
            this.tb10.Size = new System.Drawing.Size(100, 20);
            this.tb10.TabIndex = 12;
            this.tb10.Text = "15";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "До:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "От:";
            // 
            // tb21
            // 
            this.tb21.Location = new System.Drawing.Point(52, 179);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(100, 20);
            this.tb21.TabIndex = 17;
            this.tb21.Text = "50";
            // 
            // tb20
            // 
            this.tb20.Location = new System.Drawing.Point(50, 161);
            this.tb20.Name = "tb20";
            this.tb20.Size = new System.Drawing.Size(100, 20);
            this.tb20.TabIndex = 16;
            this.tb20.Text = "30";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 187);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "До:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 161);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "От:";
            // 
            // tb31
            // 
            this.tb31.Location = new System.Drawing.Point(50, 255);
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(100, 20);
            this.tb31.TabIndex = 21;
            this.tb31.Text = "60";
            // 
            // tb30
            // 
            this.tb30.Location = new System.Drawing.Point(48, 237);
            this.tb30.Name = "tb30";
            this.tb30.Size = new System.Drawing.Size(100, 20);
            this.tb30.TabIndex = 20;
            this.tb30.Text = "20";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 263);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "До:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 237);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "От:";
            // 
            // tb41
            // 
            this.tb41.Location = new System.Drawing.Point(52, 323);
            this.tb41.Name = "tb41";
            this.tb41.Size = new System.Drawing.Size(100, 20);
            this.tb41.TabIndex = 25;
            this.tb41.Text = "15.001";
            // 
            // tb40
            // 
            this.tb40.Location = new System.Drawing.Point(50, 305);
            this.tb40.Name = "tb40";
            this.tb40.Size = new System.Drawing.Size(100, 20);
            this.tb40.TabIndex = 24;
            this.tb40.Text = "15";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 331);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "До:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 305);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "От:";
            // 
            // tb51
            // 
            this.tb51.Location = new System.Drawing.Point(48, 400);
            this.tb51.Name = "tb51";
            this.tb51.Size = new System.Drawing.Size(100, 20);
            this.tb51.TabIndex = 29;
            this.tb51.Text = "30.001";
            // 
            // tb50
            // 
            this.tb50.Location = new System.Drawing.Point(46, 382);
            this.tb50.Name = "tb50";
            this.tb50.Size = new System.Drawing.Size(100, 20);
            this.tb50.TabIndex = 28;
            this.tb50.Text = "30";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 408);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(25, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "До:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 382);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "От:";
            // 
            // btnSetEvents
            // 
            this.btnSetEvents.Location = new System.Drawing.Point(17, 423);
            this.btnSetEvents.Name = "btnSetEvents";
            this.btnSetEvents.Size = new System.Drawing.Size(75, 23);
            this.btnSetEvents.TabIndex = 30;
            this.btnSetEvents.Text = "Задать";
            this.btnSetEvents.UseVisualStyleBackColor = true;
            this.btnSetEvents.Click += new System.EventHandler(this.btnSetEvents_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(246, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(174, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Максимальный размер очереди:";
            // 
            // tbMaxSize
            // 
            this.tbMaxSize.Location = new System.Drawing.Point(426, 26);
            this.tbMaxSize.Name = "tbMaxSize";
            this.tbMaxSize.Size = new System.Drawing.Size(100, 20);
            this.tbMaxSize.TabIndex = 32;
            this.tbMaxSize.Text = "300";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(249, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(142, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Интервал моделирования:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(249, 80);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Шаг отображения:";
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(426, 53);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(100, 20);
            this.tbInterval.TabIndex = 35;
            this.tbInterval.Text = "0.01";
            // 
            // tbStep
            // 
            this.tbStep.Location = new System.Drawing.Point(426, 80);
            this.tbStep.Name = "tbStep";
            this.tbStep.Size = new System.Drawing.Size(100, 20);
            this.tbStep.TabIndex = 36;
            this.tbStep.Text = "1";
            // 
            // btnQueueParams
            // 
            this.btnQueueParams.Location = new System.Drawing.Point(252, 131);
            this.btnQueueParams.Name = "btnQueueParams";
            this.btnQueueParams.Size = new System.Drawing.Size(75, 23);
            this.btnQueueParams.TabIndex = 37;
            this.btnQueueParams.Text = "Задать";
            this.btnQueueParams.UseVisualStyleBackColor = true;
            this.btnQueueParams.Click += new System.EventHandler(this.btnQueueParams_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(249, 161);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(220, 23);
            this.btnStart.TabIndex = 38;
            this.btnStart.Text = "Начать моделирование";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(252, 190);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(217, 23);
            this.btnStop.TabIndex = 39;
            this.btnStop.Text = "Остановить моделирование";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(252, 219);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(217, 23);
            this.btnDelete.TabIndex = 40;
            this.btnDelete.Text = "Удалить модель";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(252, 261);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "Результаты:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(249, 287);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(109, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "Заявок обработано:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(249, 311);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "Заявок пропущено:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(249, 331);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 44;
            this.label25.Text = "Времени прошло:";
            // 
            // lbPassed
            // 
            this.lbPassed.AutoSize = true;
            this.lbPassed.Location = new System.Drawing.Point(365, 287);
            this.lbPassed.Name = "lbPassed";
            this.lbPassed.Size = new System.Drawing.Size(0, 13);
            this.lbPassed.TabIndex = 45;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(249, 108);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(98, 13);
            this.label26.TabIndex = 46;
            this.label26.Text = "Остановка после:";
            // 
            // tbStopAfter
            // 
            this.tbStopAfter.Location = new System.Drawing.Point(426, 107);
            this.tbStopAfter.Name = "tbStopAfter";
            this.tbStopAfter.Size = new System.Drawing.Size(100, 20);
            this.tbStopAfter.TabIndex = 47;
            this.tbStopAfter.Text = "300";
            // 
            // lbDropped
            // 
            this.lbDropped.AutoSize = true;
            this.lbDropped.Location = new System.Drawing.Point(362, 311);
            this.lbDropped.Name = "lbDropped";
            this.lbDropped.Size = new System.Drawing.Size(0, 13);
            this.lbDropped.TabIndex = 48;
            // 
            // lbCurrTime
            // 
            this.lbCurrTime.AutoSize = true;
            this.lbCurrTime.Location = new System.Drawing.Point(352, 331);
            this.lbCurrTime.Name = "lbCurrTime";
            this.lbCurrTime.Size = new System.Drawing.Size(0, 13);
            this.lbCurrTime.TabIndex = 49;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(252, 356);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(148, 13);
            this.label27.TabIndex = 50;
            this.label27.Text = "Доля пропущенных заявок:";
            // 
            // lbPart
            // 
            this.lbPart.AutoSize = true;
            this.lbPart.Location = new System.Drawing.Point(410, 356);
            this.lbPart.Name = "lbPart";
            this.lbPart.Size = new System.Drawing.Size(0, 13);
            this.lbPart.TabIndex = 51;
            // 
            // lab5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 458);
            this.Controls.Add(this.lbPart);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lbCurrTime);
            this.Controls.Add(this.lbDropped);
            this.Controls.Add(this.tbStopAfter);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.lbPassed);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnQueueParams);
            this.Controls.Add(this.tbStep);
            this.Controls.Add(this.tbInterval);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tbMaxSize);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.btnSetEvents);
            this.Controls.Add(this.tb51);
            this.Controls.Add(this.tb50);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tb41);
            this.Controls.Add(this.tb40);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.tb30);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.tb20);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tb11);
            this.Controls.Add(this.tb10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tb02);
            this.Controls.Add(this.tb01);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "lab5";
            this.Text = "lab5";
            this.Load += new System.EventHandler(this.lab5_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb01;
        private System.Windows.Forms.TextBox tb02;
        private System.Windows.Forms.TextBox tb11;
        private System.Windows.Forms.TextBox tb10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.TextBox tb20;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb31;
        private System.Windows.Forms.TextBox tb30;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb41;
        private System.Windows.Forms.TextBox tb40;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb51;
        private System.Windows.Forms.TextBox tb50;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnSetEvents;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbMaxSize;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.TextBox tbStep;
        private System.Windows.Forms.Button btnQueueParams;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbPassed;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbStopAfter;
        private System.Windows.Forms.Label lbDropped;
        private System.Windows.Forms.Label lbCurrTime;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lbPart;
    }
}

