﻿using Common;
using QueueAnalyzer.EventGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueAnalyzer
{
    sealed class EventRunner
    {
        public static int GeneratorsLength { get; } = 6;

        public IList<EventGenerator> Generators
        {
            get
            {
                return generators;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Generators");
                if (value.Count != GeneratorsLength)
                    throw new ArgumentException("Generators");
                generators = value;
            }
        }
        private IList<EventGenerator> generators;

        public int MaxSize
        {
            get
            {
                return maxSize;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("MaxSize");
                maxSize = value;
            }
        }
        private int maxSize;

        public double Interval
        {
            get
            {
                return interval;
            }
            set
            {
                if (value < 0 || value.Compare(0))
                    throw new ArgumentOutOfRangeException("Interval");
                interval = value;
            }
        }
        private double interval;

        public long Iterations { get; private set; } = 0;

        public double CurrTime { get; private set; } = 0;

        public double?[] processTime = new double?[GeneratorsLength];

        private int passed = 0;

        private int dropped = 0;

        private int queue1 = 0;

        private int queue2 = 0;

        public EventRunner(IList<EventGenerator> generators, int maxSize, double interval)
        {
            Generators = generators;
            MaxSize = maxSize;
            Interval = interval;

            processTime[0] = Generators[0].NextEvent(0);
        }

        public void Step()
        {
            StartProcessing();
            ProcessQueues();
            AddEvents();
            UpdateData();
        }

        private void UpdateData()
        {
            CurrTime += Interval;
            ++Iterations;
        }

        private void AddEvents()
        {
            if (processTime[0] < CurrTime)
            {
                double currAverage = Generators.Max(x => x.Average);
                int curr = -1;
                for (int i = 1; i < 4; ++i)
                    if (processTime[i] == null && Generators[i].Average <= currAverage)
                    {
                        curr = i;
                        currAverage = Generators[i].Average;
                    }
                if (curr != -1)
                    processTime[curr] = Generators[curr].NextEvent(CurrTime);
                else
                    ++dropped;
                processTime[0] = Generators[0].NextEvent(CurrTime);
            }
        }

        private void ProcessQueues()
        {
            for (int i = 1; i < 3; ++i)
                if (processTime[i].HasValue && processTime[i] < CurrTime)
                {
                    AddToQueue(ref queue1);
                    processTime[i] = null;
                }
            if (processTime[3].HasValue && processTime[3] < CurrTime)
            {
                AddToQueue(ref queue2);
                processTime[3] = null;
            }
            if (processTime[4].HasValue && processTime[4] < CurrTime)
            {
                ++passed;
                --queue1;
                processTime[4] = null;
            }
            if (processTime[5].HasValue && processTime[5] < CurrTime)
            {
                ++passed;
                --queue2;
                processTime[5] = null;
            }
        }

        private void StartProcessing()
        {
            if (queue1 != 0 && processTime[4] == null)
                processTime[4] = Generators[4].NextEvent(CurrTime);
            if (queue2 != 0 && processTime[5] == null)
                processTime[5] = Generators[5].NextEvent(CurrTime);
        }

        private void AddToQueue(ref int size)
        {
            if (size < MaxSize)
                ++size;
            else
                ++dropped;
        }

        public QueueStats GetStats()
        {
            return new QueueStats(passed, 0, dropped, 0, CurrTime, null);
        }
    }
}
