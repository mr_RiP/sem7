﻿using Common;
using ProbabilitiesCalculator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Lab2 : Form
    {
        public Lab2()
        {
            InitializeComponent();
        }

        private Random random = new Random();

        private void btnSetStates_Click(object sender, EventArgs e)
        {
            int value = (int)nudStates.Value;
            SetStateCount(value);
        }

        private void SetStateCount(int value)
        {
            SetGrid(dgvStates, value, value);
            SetGrid(dgvResults, value, 1);
            SetRowsNames(dgvStates);
            SetReadOnlyCells();
            dgvStates.DisableSorting();
            dgvResults.DisableSorting();
        }

        private void SetReadOnlyCells()
        {
            for (int i = 0; i < dgvStates.ColumnCount; ++i)
                dgvStates[i, i].ReadOnly = true;
        }

        private static void SetGrid(DataGridView dgv, int cols, int rows)
        {
            if (cols < 1)
                throw new ArgumentOutOfRangeException("cols");
            if (rows < 1)
                throw new ArgumentOutOfRangeException("rows");

            dgv.Clear();
            if (cols < dgv.ColumnCount)
                for (int i = dgv.ColumnCount - 1; i >= cols; --i)
                    dgv.Columns.RemoveAt(i);
            else
            {
                var columns = Enumerable.Range(dgv.ColumnCount, cols - dgv.ColumnCount)
                    .Select(ind => new DataGridViewTextBoxColumn() { HeaderText = $"s{ind + 1}" })
                    .ToArray();
                dgv.Columns.AddRange(columns);
            }
            dgv.RowCount = rows;
        }

        private static void SetRowsNames(DataGridView dgv)
        {
            for (int i = 0; i < dgv.RowCount; ++i)
                dgv.Rows[i].HeaderCell.Value = $"s{i + 1}";
        }

        private void lab3_Load(object sender, EventArgs e)
        {
            SetStateCount(2);
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            var states = ParseStates();
            if (states == null)
            {
                MessageBox.Show("Матрица заполнена неверно!");
                return;
            }

            var calc = new ProbCalculator(states);
            var res = calc.GetProbabilities();
            if (res != null)
                for (int i = 0; i < res.Count; ++i)
                    dgvResults[i, 0].Value = res[i].ToString();
        }

        private double[,] ParseStates()
        {
            int size = dgvStates.ColumnCount;

            double[,] values = new double[size, size];
            for (int i = 0; i < size; ++i)
                for (int j = 0; j < size; ++j)
                {
                    if (i != j)
                    {
                        double value;
                        if (double.TryParse(dgvStates[j, i].Value as string, out value))
                            values[i, j] = value;
                        else
                            return null;
                    }
                    else
                        values[i, j] = 0;
                }
            return values;
        }

        private void btnRandom_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvStates.RowCount; ++i)
                for (int j = 0; j < dgvStates.ColumnCount; ++j)
                    if (i != j)
                        dgvStates[j, i].Value = random.NextDouble().ToString();
        }
    }
}
