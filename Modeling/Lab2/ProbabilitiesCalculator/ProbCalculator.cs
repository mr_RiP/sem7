﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbabilitiesCalculator
{
    public sealed class ProbCalculator
    {
        private readonly double[,] matrix;

        private readonly int size;

        private IList<double> result = null;

        private IList<double> times = null;

        private bool isCalculated = false;

        public ProbCalculator(double[,] source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (source.Length == 0 || source.Rank != 2)
                throw new ArgumentException("source");

            this.size = source.GetLength(0);

            if (size != source.GetLength(1))
                throw new ArgumentException("source");

            this.matrix = source;
        }

        public IList<double> GetProbabilities()
        {
            TryCalculation();
            return result;
        }

        public IList<double> GetTimes()
        {
            TryCalculation();
            return times;
        }

        private void TryCalculation()
        {
            try
            {
                if (!isCalculated)
                    Calculate();
            }
            catch (Exception)
            {
                result = null;
                times = null;
            }
            finally
            {
                isCalculated = true;
            }
        }

        private void Calculate()
        {
            double[][] equations = new double[size][];
            double[] answers = new double[size];
            for (int i = 0; i < size - 1; ++i)
            {
                equations[i] = new double[size];
                for (int j = 0; j < size; ++j)
                    equations[i][j] = matrix[j, i];

                double sum = 0;
                for (int j = 0; j < size; ++j)
                    sum += matrix[i, j];
                equations[i][i] = -sum;

                answers[i] = 0;
            }
            equations[size - 1] = new double[size];
            for (int i = 0; i < size; ++i)
                equations[size - 1][i] = 1;
            answers[size - 1] = 1;
            this.result = new GaussCalculator(equations, answers).GetSolution();
            CalculateTimes();
        }

        private void CalculateTimes()
        {
            if (result == null)
                return;

            times = new double[size];
            for (int i = 0; i < size; ++i)
            {
                double sum = 0;
                for (int j = 0; j < size; ++j)
                    sum += matrix[i, j];
                times[i] = result[i] / sum;
            }
        }

        private double GetColumnSum(int column)
        {
            double res = 0;
            for (int i = 0; i < size; ++i)
                res += matrix[i, column];
            return res;
        }

        private double GetRowSum(int row)
        {
            double res = 0;
            for (int j = 0; j < size; ++j)
                res += matrix[row, j];
            return res;
        }
    }
}