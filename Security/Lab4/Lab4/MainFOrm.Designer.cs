﻿namespace Lab4
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.grpCipherFile = new System.Windows.Forms.GroupBox();
            this.txtCipherFileName = new System.Windows.Forms.TextBox();
            this.btnOpenCipherFile = new System.Windows.Forms.Button();
            this.grpMessageFile = new System.Windows.Forms.GroupBox();
            this.txtMessageFileName = new System.Windows.Forms.TextBox();
            this.btnOpenMessageFile = new System.Windows.Forms.Button();
            this.groupSettings = new System.Windows.Forms.GroupBox();
            this.lblD = new System.Windows.Forms.Label();
            this.lblN = new System.Windows.Forms.Label();
            this.txtD = new System.Windows.Forms.TextBox();
            this.lblE = new System.Windows.Forms.Label();
            this.txtE = new System.Windows.Forms.TextBox();
            this.txtQ = new System.Windows.Forms.TextBox();
            this.txtP = new System.Windows.Forms.TextBox();
            this.txtN = new System.Windows.Forms.TextBox();
            this.lblQ = new System.Windows.Forms.Label();
            this.lvlP = new System.Windows.Forms.Label();
            this.btnEncipher = new System.Windows.Forms.Button();
            this.btnDecipher = new System.Windows.Forms.Button();
            this.btnGenerateKeys = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnUnlockSettings = new System.Windows.Forms.Button();
            this.grpFiles.SuspendLayout();
            this.grpCipherFile.SuspendLayout();
            this.grpMessageFile.SuspendLayout();
            this.groupSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.grpCipherFile);
            this.grpFiles.Controls.Add(this.grpMessageFile);
            this.grpFiles.Location = new System.Drawing.Point(12, 12);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(760, 125);
            this.grpFiles.TabIndex = 0;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "Файлы";
            // 
            // grpCipherFile
            // 
            this.grpCipherFile.Controls.Add(this.txtCipherFileName);
            this.grpCipherFile.Controls.Add(this.btnOpenCipherFile);
            this.grpCipherFile.Location = new System.Drawing.Point(6, 72);
            this.grpCipherFile.Name = "grpCipherFile";
            this.grpCipherFile.Size = new System.Drawing.Size(748, 48);
            this.grpCipherFile.TabIndex = 1;
            this.grpCipherFile.TabStop = false;
            this.grpCipherFile.Text = "Шифр";
            // 
            // txtCipherFileName
            // 
            this.txtCipherFileName.Location = new System.Drawing.Point(6, 19);
            this.txtCipherFileName.Name = "txtCipherFileName";
            this.txtCipherFileName.ReadOnly = true;
            this.txtCipherFileName.Size = new System.Drawing.Size(655, 20);
            this.txtCipherFileName.TabIndex = 3;
            // 
            // btnOpenCipherFile
            // 
            this.btnOpenCipherFile.Location = new System.Drawing.Point(667, 17);
            this.btnOpenCipherFile.Name = "btnOpenCipherFile";
            this.btnOpenCipherFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenCipherFile.TabIndex = 2;
            this.btnOpenCipherFile.Text = "Открыть";
            this.btnOpenCipherFile.UseVisualStyleBackColor = true;
            this.btnOpenCipherFile.Click += new System.EventHandler(this.btnOpenCipherFile_Click);
            // 
            // grpMessageFile
            // 
            this.grpMessageFile.Controls.Add(this.txtMessageFileName);
            this.grpMessageFile.Controls.Add(this.btnOpenMessageFile);
            this.grpMessageFile.Location = new System.Drawing.Point(6, 19);
            this.grpMessageFile.Name = "grpMessageFile";
            this.grpMessageFile.Size = new System.Drawing.Size(748, 47);
            this.grpMessageFile.TabIndex = 0;
            this.grpMessageFile.TabStop = false;
            this.grpMessageFile.Text = "Сообщение";
            // 
            // txtMessageFileName
            // 
            this.txtMessageFileName.Location = new System.Drawing.Point(6, 19);
            this.txtMessageFileName.Name = "txtMessageFileName";
            this.txtMessageFileName.ReadOnly = true;
            this.txtMessageFileName.Size = new System.Drawing.Size(655, 20);
            this.txtMessageFileName.TabIndex = 1;
            // 
            // btnOpenMessageFile
            // 
            this.btnOpenMessageFile.Location = new System.Drawing.Point(667, 17);
            this.btnOpenMessageFile.Name = "btnOpenMessageFile";
            this.btnOpenMessageFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenMessageFile.TabIndex = 0;
            this.btnOpenMessageFile.Text = "Открыть";
            this.btnOpenMessageFile.UseVisualStyleBackColor = true;
            this.btnOpenMessageFile.Click += new System.EventHandler(this.btnOpenMessageFile_Click);
            // 
            // groupSettings
            // 
            this.groupSettings.Controls.Add(this.lblD);
            this.groupSettings.Controls.Add(this.lblN);
            this.groupSettings.Controls.Add(this.txtD);
            this.groupSettings.Controls.Add(this.lblE);
            this.groupSettings.Controls.Add(this.txtE);
            this.groupSettings.Controls.Add(this.txtQ);
            this.groupSettings.Controls.Add(this.txtP);
            this.groupSettings.Controls.Add(this.txtN);
            this.groupSettings.Controls.Add(this.lblQ);
            this.groupSettings.Controls.Add(this.lvlP);
            this.groupSettings.Location = new System.Drawing.Point(12, 143);
            this.groupSettings.Name = "groupSettings";
            this.groupSettings.Size = new System.Drawing.Size(487, 62);
            this.groupSettings.TabIndex = 1;
            this.groupSettings.TabStop = false;
            this.groupSettings.Text = "Настройки";
            // 
            // lblD
            // 
            this.lblD.AutoSize = true;
            this.lblD.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblD.Location = new System.Drawing.Point(430, 16);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(14, 15);
            this.lblD.TabIndex = 11;
            this.lblD.Text = "d";
            // 
            // lblN
            // 
            this.lblN.AutoSize = true;
            this.lblN.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblN.Location = new System.Drawing.Point(331, 16);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(15, 15);
            this.lblN.TabIndex = 12;
            this.lblN.Text = "n";
            // 
            // txtD
            // 
            this.txtD.Location = new System.Drawing.Point(390, 34);
            this.txtD.Name = "txtD";
            this.txtD.ReadOnly = true;
            this.txtD.Size = new System.Drawing.Size(90, 20);
            this.txtD.TabIndex = 2;
            // 
            // lblE
            // 
            this.lblE.AutoSize = true;
            this.lblE.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblE.Location = new System.Drawing.Point(236, 16);
            this.lblE.Name = "lblE";
            this.lblE.Size = new System.Drawing.Size(13, 15);
            this.lblE.TabIndex = 7;
            this.lblE.Text = "e";
            // 
            // txtE
            // 
            this.txtE.Location = new System.Drawing.Point(198, 34);
            this.txtE.Name = "txtE";
            this.txtE.ReadOnly = true;
            this.txtE.Size = new System.Drawing.Size(90, 20);
            this.txtE.TabIndex = 13;
            // 
            // txtQ
            // 
            this.txtQ.Location = new System.Drawing.Point(102, 34);
            this.txtQ.Name = "txtQ";
            this.txtQ.ReadOnly = true;
            this.txtQ.Size = new System.Drawing.Size(90, 20);
            this.txtQ.TabIndex = 8;
            // 
            // txtP
            // 
            this.txtP.Location = new System.Drawing.Point(6, 34);
            this.txtP.Name = "txtP";
            this.txtP.ReadOnly = true;
            this.txtP.Size = new System.Drawing.Size(90, 20);
            this.txtP.TabIndex = 7;
            // 
            // txtN
            // 
            this.txtN.Location = new System.Drawing.Point(294, 34);
            this.txtN.Name = "txtN";
            this.txtN.ReadOnly = true;
            this.txtN.Size = new System.Drawing.Size(90, 20);
            this.txtN.TabIndex = 10;
            // 
            // lblQ
            // 
            this.lblQ.AutoSize = true;
            this.lblQ.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQ.Location = new System.Drawing.Point(142, 16);
            this.lblQ.Name = "lblQ";
            this.lblQ.Size = new System.Drawing.Size(14, 15);
            this.lblQ.TabIndex = 6;
            this.lblQ.Text = "q";
            // 
            // lvlP
            // 
            this.lvlP.AutoSize = true;
            this.lvlP.Font = new System.Drawing.Font("Cambria", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvlP.Location = new System.Drawing.Point(42, 16);
            this.lvlP.Name = "lvlP";
            this.lvlP.Size = new System.Drawing.Size(14, 15);
            this.lvlP.TabIndex = 5;
            this.lvlP.Text = "p";
            // 
            // btnEncipher
            // 
            this.btnEncipher.Location = new System.Drawing.Point(640, 151);
            this.btnEncipher.Name = "btnEncipher";
            this.btnEncipher.Size = new System.Drawing.Size(129, 23);
            this.btnEncipher.TabIndex = 14;
            this.btnEncipher.Text = "Зашифровать";
            this.btnEncipher.UseVisualStyleBackColor = true;
            this.btnEncipher.Click += new System.EventHandler(this.btnEncipher_Click);
            // 
            // btnDecipher
            // 
            this.btnDecipher.Location = new System.Drawing.Point(640, 180);
            this.btnDecipher.Name = "btnDecipher";
            this.btnDecipher.Size = new System.Drawing.Size(129, 23);
            this.btnDecipher.TabIndex = 15;
            this.btnDecipher.Text = "Расшифровать";
            this.btnDecipher.UseVisualStyleBackColor = true;
            this.btnDecipher.Click += new System.EventHandler(this.btnDecipher_Click);
            // 
            // btnGenerateKeys
            // 
            this.btnGenerateKeys.Location = new System.Drawing.Point(505, 151);
            this.btnGenerateKeys.Name = "btnGenerateKeys";
            this.btnGenerateKeys.Size = new System.Drawing.Size(129, 23);
            this.btnGenerateKeys.TabIndex = 16;
            this.btnGenerateKeys.Text = "Сгенерировать ключи";
            this.btnGenerateKeys.UseVisualStyleBackColor = true;
            this.btnGenerateKeys.Click += new System.EventHandler(this.btnGenerateKeys_Click);
            // 
            // btnUnlockSettings
            // 
            this.btnUnlockSettings.Location = new System.Drawing.Point(505, 180);
            this.btnUnlockSettings.Name = "btnUnlockSettings";
            this.btnUnlockSettings.Size = new System.Drawing.Size(129, 23);
            this.btnUnlockSettings.TabIndex = 17;
            this.btnUnlockSettings.Text = "Изменить ключи";
            this.btnUnlockSettings.UseVisualStyleBackColor = true;
            this.btnUnlockSettings.Click += new System.EventHandler(this.btnChangeKey_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 216);
            this.Controls.Add(this.btnUnlockSettings);
            this.Controls.Add(this.btnGenerateKeys);
            this.Controls.Add(this.btnDecipher);
            this.Controls.Add(this.btnEncipher);
            this.Controls.Add(this.groupSettings);
            this.Controls.Add(this.grpFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "RSA";
            this.grpFiles.ResumeLayout(false);
            this.grpCipherFile.ResumeLayout(false);
            this.grpCipherFile.PerformLayout();
            this.grpMessageFile.ResumeLayout(false);
            this.grpMessageFile.PerformLayout();
            this.groupSettings.ResumeLayout(false);
            this.groupSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.GroupBox grpCipherFile;
        private System.Windows.Forms.TextBox txtCipherFileName;
        private System.Windows.Forms.Button btnOpenCipherFile;
        private System.Windows.Forms.GroupBox grpMessageFile;
        private System.Windows.Forms.TextBox txtMessageFileName;
        private System.Windows.Forms.Button btnOpenMessageFile;
        private System.Windows.Forms.GroupBox groupSettings;
        private System.Windows.Forms.Label lblE;
        private System.Windows.Forms.Label lblQ;
        private System.Windows.Forms.Label lvlP;
        private System.Windows.Forms.TextBox txtD;
        private System.Windows.Forms.TextBox txtN;
        private System.Windows.Forms.Label lblD;
        private System.Windows.Forms.Label lblN;
        private System.Windows.Forms.Button btnEncipher;
        private System.Windows.Forms.Button btnDecipher;
        private System.Windows.Forms.Button btnGenerateKeys;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox txtE;
        private System.Windows.Forms.TextBox txtP;
        private System.Windows.Forms.TextBox txtQ;
        private System.Windows.Forms.Button btnUnlockSettings;
    }
}

