﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeCipherer();
        }

        private void btnOpenMessageFile_Click(object sender, EventArgs e)
        {
            SetFile(txtMessageFileName, txtCipherFileName);
        }

        private void btnOpenCipherFile_Click(object sender, EventArgs e)
        {
            SetFile(txtCipherFileName, txtMessageFileName);
        }

        private void btnGenerateKeys_Click(object sender, EventArgs e)
        {
            GenerateRandomSettings();
        }

        private void btnEncipher_Click(object sender, EventArgs e)
        {
            ProcessFiles(txtMessageFileName.Text, txtCipherFileName.Text, bytes => cipherer.Encipher(bytes));
        }

        private void btnDecipher_Click(object sender, EventArgs e)
        {
            ProcessFiles(txtCipherFileName.Text, txtMessageFileName.Text, bytes => cipherer.Decipher(bytes));
        }

        private void btnChangeKey_Click(object sender, EventArgs e)
        {
            ToggleManualMode();
        }
    }
}
