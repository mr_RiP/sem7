﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class MainForm
    {
        private Rsa cipherer;
        private RandomPrimesGenerator randomGenerator;

        private void InitializeCipherer()
        {          
            randomGenerator = new RandomPrimesGenerator(10000);

            Tuple<int, int, int> values = GetDistinctRandomPrimes();
            cipherer = new Rsa(values.Item1, values.Item2, values.Item3);

            PrintSettings();
        }

        private Tuple<int, int, int> GetDistinctRandomPrimes()
        {
            int p = randomGenerator.GetRandomPrime();
            int q = randomGenerator.GetRandomPrime(p);
            int e = randomGenerator.GetRandomPrime(p, q);
            while (BigInteger.Multiply(p, q) < e)
                e = randomGenerator.GetRandomPrime(p, q);

            return new Tuple<int, int, int>(p, q, e);
        }

        private void GenerateRandomSettings()
        {
            Tuple<int, int, int> values = GetDistinctRandomPrimes();
            cipherer.P = values.Item1;
            cipherer.Q = values.Item2;
            cipherer.E = values.Item3;

            cipherer.GenerateKeys();

            PrintSettings();
        }

        private void UpdateKeys()
        {
            try
            {
                int e = int.Parse(txtE.Text);
                BigInteger n = BigInteger.Parse(txtN.Text);
                BigInteger d = BigInteger.Parse(txtD.Text);

                if (e < 1 || n < 1 || d < 1)
                    throw new Exception();

                cipherer.E = e;
                cipherer.N = n;
                cipherer.D = d;
            }
            catch (Exception)
            {
                MessageBox.Show("Введены некорректные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                PrintKeys();
            }
        }
    }
}
