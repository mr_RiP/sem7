﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    partial class MainForm
    {
        private Aes aes = null;

        private void InitializeAes()
        {
            if (aes == null)
                aes = new Aes(AesStandard.Aes128, GetKeyValues());
            else
                aes.Key = GetKeyValues();
        }

        private void EncipherFile()
        {
            ProcessFiles(txtMessageFileName.Text, txtCipherFileName.Text, buf => aes.Encipher(buf));
        }

        private void DecipherFile()
        {
            ProcessFiles(txtCipherFileName.Text, txtMessageFileName.Text, buf => aes.Decipher(buf));
        }

        private void ProcessFiles(string inputFile, string outputFile, Func<byte[],byte[]> func)
        {
            int bufferLength = 16;

            try
            {
                using (var reader = new BinaryReader(File.OpenRead(inputFile)))
                using (var writer = new BinaryWriter(File.OpenWrite(outputFile)))
                {
                    byte[] buffer = reader.ReadBytes(bufferLength);
                    while (buffer.Length > 0)
                    {
                        byte[] result = func(buffer);

                        writer.Write(result);
                        buffer = reader.ReadBytes(bufferLength);
                    }
                }

                ShowOperationSuccessMessage();              
            }
            catch (Exception e)
            {
                ShowErrorMessage(e.Message);
            }
        }

        private void ShowOperationSuccessMessage()
        {
            MessageBox.Show("Операция успешно завершена.", "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ShowErrorMessage(string exceptionMessage)
        {
            MessageBox.Show("Во время выполнения операции произошла ошибка:\n" + exceptionMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
