﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public enum AesStandard
    {
        Aes128,
        Aes192,
        Aes256
    }

    public class Aes
    {
        public byte[] Key
        {
            get
            {
                return cipherKey;
            }

            set
            {
                int keyLength = stdNb * stdNk;

                if (value == null)
                    throw new ArgumentNullException();

                if (value.Length != keyLength)
                    throw new ArgumentException("Key array length must be " + keyLength + ".");

                cipherKey = value.Clone() as byte[];
            }
        }

        public Aes(AesStandard standard, byte[] key) : this()
        {
            InitializeStandardValues(standard);
            Key = key;
        }

        public byte[] Encipher(byte[] bytes)
        {
            byte[][] state = SetupBlock(bytes);           
            byte[][] roundKeys = GenerateRoundKeys();

            AddRoundKey(state, roundKeys, 0);
            for (int i = 1; i < stdNr; ++i)
            {
                SubBytes(state);
                ShiftRows(state);
                MixColumns(state);
                AddRoundKey(state, roundKeys, i);
            }
            SubBytes(state);
            ShiftRows(state);
            AddRoundKey(state, roundKeys, stdNr);

            return GetBytes(state);
        }

        public byte[] Decipher(byte[] bytes)
        {
            byte[][] state = SetupBlock(bytes);
            byte[][] roundKeys = GenerateRoundKeys();

            AddRoundKey(state, roundKeys, stdNr);

            for (int i = stdNr - 1; i > 0; --i)
            {
                InvShiftRows(state);
                InvSubBytes(state);
                AddRoundKey(state, roundKeys, i);
                InvMixColumns(state);
            }
            InvShiftRows(state);
            InvSubBytes(state);
            AddRoundKey(state, roundKeys, 0);

            return GetBytes(state);
        }

        public static byte[] GenerateRandomKey(AesStandard standard)
        {
            var key = new byte[GetKeyLength(standard)];
            var random = new Random();
            random.NextBytes(key);

            return key;
        }

        private const int stdNb = 4;
        private int stdNk = 4;
        private int stdNr = 10;

        private byte[] cipherKey;
        private byte[] sBox;
        private byte[] invSBox;
        private byte[][] rCon;
        private Galois gal;

        private Aes()
        {
            InitializeSBox();
            InitializeInvSBox();
            InitializeRCon();
            InitializeGalois();
        }

        private void InitializeStandardValues(AesStandard standard)
        {
            switch (standard)
            {
                case AesStandard.Aes128:
                    stdNk = 4;
                    stdNr = 10;
                    break;
                case AesStandard.Aes192:
                    stdNk = 6;
                    stdNr = 12;
                    break;
                case AesStandard.Aes256:
                    stdNk = 8;
                    stdNr = 14;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private static int GetKeyLength(AesStandard standard)
        {
            switch (standard)
            {
                case AesStandard.Aes128:
                    return 16;
                case AesStandard.Aes192:
                    return 24;
                case AesStandard.Aes256:
                    return 32;
                default:
                    throw new NotImplementedException();
            }
        }

        private void InitializeGalois()
        {
            gal = new Galois();
        }

        private void InitializeInvSBox()
        {
            invSBox = new byte[]
            {
                0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
                0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
                0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
                0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
                0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
                0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
                0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
                0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
                0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
                0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
                0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
                0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
                0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
                0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
                0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
                0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
            };
        }

        private void InitializeSBox()
        {
            sBox = new byte[]
            {
                0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
            };
        }

        private void InitializeRCon()
        {
            rCon = new byte[][] 
            {
                new byte[] {0x00, 0x00, 0x00, 0x00},
                new byte[] {0x01, 0x00, 0x00, 0x00},
                new byte[] {0x02, 0x00, 0x00, 0x00},
                new byte[] {0x04, 0x00, 0x00, 0x00},
                new byte[] {0x08, 0x00, 0x00, 0x00},
                new byte[] {0x10, 0x00, 0x00, 0x00},
                new byte[] {0x20, 0x00, 0x00, 0x00},
                new byte[] {0x40, 0x00, 0x00, 0x00},
                new byte[] {0x80, 0x00, 0x00, 0x00},
                new byte[] {0x1b, 0x00, 0x00, 0x00},
                new byte[] {0x36, 0x00, 0x00, 0x00}
            };
        }

        private byte[][] SetupBlock(byte[] bytes)
        {
            int blockSize = stdNb * stdNb;

            if (bytes == null)
                throw new ArgumentNullException("bytes");

            if (bytes.Length > blockSize)
                throw new ArgumentOutOfRangeException("bytes", "Array length must be " + blockSize + " or less.");

            var state = new byte[stdNb][];
            for (int i = 0; i < stdNb; ++i)
                state[i] = new byte[stdNb];

            for (int i = 0; i < stdNb; ++i)
            {
                for (int j = 0; j < stdNb; ++j)
                {
                    int k = i * stdNb + j;
                    state[j][i] = k < bytes.Length ? bytes[k] : (byte)0;
                }
            }

            return state;
        }

        private byte[] GetBytes(byte[][] state)
        {
            var bytes = new byte[stdNb * stdNb];
            for (int i = 0; i < stdNb; ++i)
                for (int j = 0; j < stdNb; ++j)
                    bytes[i * stdNb + j] = state[j][i];

            return bytes;
        }

        private byte[][] GenerateRoundKeys()
        {
            var words = new byte[stdNb * (stdNr + 1)][];

            for (int i = 0; i < stdNk; ++i)
                words[i] = new byte[stdNb] 
                {
                    cipherKey[i * stdNb],
                    cipherKey[i * stdNb + 1],
                    cipherKey[i * stdNb + 2],
                    cipherKey[i * stdNb + 3]
                };

            for (int i = stdNk; i < words.Length; ++i)
            {
                byte[] temp = words[i - 1];

                if (i % stdNk == 0)
                    temp = XorWords(SubWord(RotWord(temp)), rCon[i / stdNk]);
                else if (stdNk > 6 && i % stdNk == 4)
                    temp = SubWord(temp);

                words[i] = XorWords(words[i - stdNk], temp);
            }

            return words;
        }

        private byte[] SubWord(byte[] word)
        {
            var newWord = new byte[stdNb];
            for (int i = 0; i < stdNb; ++i)
                newWord[i] = sBox[word[i]];

            return newWord;
        }

        private byte[] RotWord(byte[] word)
        {
            var newWord = new byte[stdNb];
            newWord[stdNb - 1] = word[0];
            for (int i = 1; i < word.Length; ++i)
                newWord[i - 1] = word[i];

            return newWord;
        }

        private void AddRoundKey(byte[][] state, byte[][] roundKeys, int roundIndex)
        {
            int roundKeyBegin = roundIndex * stdNb;
            for (int i = 0; i < stdNb; ++i)
                state[i] = XorWords(state[i], roundKeys[roundKeyBegin + i]);
        }

        private byte[] XorWords(byte[] aWord, byte[] bWord)
        {
            int a = BitConverter.ToInt32(aWord, 0);
            int b = BitConverter.ToInt32(bWord, 0);
            int result = a ^ b;
            return BitConverter.GetBytes(result);
        }

        private byte[] XorWords(byte[] aWord, int b)
        {
            int a = BitConverter.ToInt32(aWord, 0);
            int result = a ^ b;
            return BitConverter.GetBytes(result);
        }

        private byte[] XorWords(int a, byte[] bWord)
        {
            return XorWords(bWord, a);
        }

        private void ShiftRows(byte[][] state)
        {
            for (int i = 1; i < stdNb; ++i)
                for (int j = 0; j < i; ++j)
                    state[i] = RotWord(state[i]);
        }

        private void MixColumns(byte[][] state)
        {
            var temp = new byte[stdNb][];
            for (int i = 0; i < stdNb; ++i)
                temp[i] = new byte[stdNb];

            for (int i = 0; i < stdNb; ++i)
            {
                temp[0][i] = (byte)(gal.MultiplyBy2(state[0][i]) ^ gal.MultiplyBy3(state[1][i]) ^ state[2][i] ^ state[3][i]);
                temp[1][i] = (byte)(state[0][i] ^ gal.MultiplyBy2(state[1][i]) ^ gal.MultiplyBy3(state[2][i]) ^ state[3][i]);
                temp[2][i] = (byte)(state[0][i] ^ state[1][i] ^ gal.MultiplyBy2(state[2][i]) ^ gal.MultiplyBy3(state[3][i]));
                temp[3][i] = (byte)(gal.MultiplyBy3(state[0][i]) ^ state[1][i] ^ state[2][i] ^ gal.MultiplyBy2(state[3][i]));
            }

            for (int i = 0; i < stdNb; ++i)
                state[i] = temp[i];
        }

        private void InvMixColumns(byte[][] state)
        {
            var temp = new byte[stdNb][];
            for (int i = 0; i < stdNb; ++i)
                temp[i] = new byte[stdNb];

            for (int i = 0; i < stdNb; ++i)
            {
                temp[0][i] = (byte)(gal.MultiplyBy14(state[0][i]) ^ gal.MultiplyBy11(state[1][i]) ^ gal.MultiplyBy13(state[2][i]) ^ gal.MultiplyBy9(state[3][i]));
                temp[1][i] = (byte)(gal.MultiplyBy9(state[0][i]) ^ gal.MultiplyBy14(state[1][i]) ^ gal.MultiplyBy11(state[2][i]) ^ gal.MultiplyBy13(state[3][i]));
                temp[2][i] = (byte)(gal.MultiplyBy13(state[0][i]) ^ gal.MultiplyBy9(state[1][i]) ^ gal.MultiplyBy14(state[2][i]) ^ gal.MultiplyBy11(state[3][i]));
                temp[3][i] = (byte)(gal.MultiplyBy11(state[0][i]) ^ gal.MultiplyBy13(state[1][i]) ^ gal.MultiplyBy9(state[2][i]) ^ gal.MultiplyBy14(state[3][i]));
            }

            for (int i = 0; i < stdNb; ++i)
                state[i] = temp[i];
        }

        private void SubBytes(byte[][] state)
        {
            for (int i = 0; i < stdNb; ++i)
                state[i] = SubWord(state[i]);
        }

        private void InvSubBytes(byte[][] state)
        {
            for (int i = 0; i < stdNb; ++i)
                for (int j = 0; j < stdNb; ++j)
                    state[i][j] = invSBox[state[i][j]];
        }

        private void InvShiftRows(byte[][] state)
        {
            for (int i = 1; i < stdNb; ++i)
            {
                var row = state[i];
                for (int j = 0; j < i; ++j)
                {
                    byte temp = row[stdNb - 1];
                    for (int k = stdNb - 1; k > 0; --k)
                        row[k] = row[k - 1];
                    row[0] = temp;
                }
            }
        }
    }
}
