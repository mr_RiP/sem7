﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public static class DebugBytes
    {
        public static void WriteState(byte[][] state)
        {
            StringBuilder hex = new StringBuilder();
            foreach (byte[] word in state)
            {
                foreach (byte value in word)
                    hex.AppendFormat("{0:x2} ", value);
                hex.Append("   ");
            }

            Debug.WriteLine(hex.ToString());
        }

        public static void WriteStateMessage(byte[][] state, string message)
        {
            Debug.Write(message + ":\t\t\t");
            DebugBytes.WriteState(state);
        }
    }
}
