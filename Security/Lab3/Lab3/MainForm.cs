﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeKeyGrid();
        }

        private void btnSetMessageFile_Click(object sender, EventArgs e)
        {
            SetFile(txtMessageFileName, txtCipherFileName);
        }

        private void btnSetCipherFile_Click(object sender, EventArgs e)
        {
            SetFile(txtCipherFileName, txtMessageFileName);
        }

        private void btnSetRandomKey_Click(object sender, EventArgs e)
        {
            SetKeyGridValuesRandom();
        }

        private void gridKey_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            SaveKeyGridCellValue(e.ColumnIndex, e.RowIndex);
        }

        private void gridKey_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            SetKeyGridCellValue(e.ColumnIndex, e.RowIndex);
        }

        private void btnStartCiphering_Click(object sender, EventArgs e)
        {
            InitializeAes();
            EncipherFile();
        }

        private void btnStartDeciphering_Click(object sender, EventArgs e)
        {
            InitializeAes();
            DecipherFile();
        }
    }
}
