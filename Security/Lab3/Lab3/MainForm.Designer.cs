﻿namespace Lab3
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMessageFileName = new System.Windows.Forms.TextBox();
            this.txtCipherFileName = new System.Windows.Forms.TextBox();
            this.btnSetMessageFile = new System.Windows.Forms.Button();
            this.btnSetCipherFile = new System.Windows.Forms.Button();
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.grpCipherFile = new System.Windows.Forms.GroupBox();
            this.grpMessageFile = new System.Windows.Forms.GroupBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnStartCiphering = new System.Windows.Forms.Button();
            this.btnStartDeciphering = new System.Windows.Forms.Button();
            this.grpKey = new System.Windows.Forms.GroupBox();
            this.gridKey = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSetRandomKey = new System.Windows.Forms.Button();
            this.grpFiles.SuspendLayout();
            this.grpCipherFile.SuspendLayout();
            this.grpMessageFile.SuspendLayout();
            this.grpKey.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridKey)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMessageFileName
            // 
            this.txtMessageFileName.Location = new System.Drawing.Point(6, 19);
            this.txtMessageFileName.Name = "txtMessageFileName";
            this.txtMessageFileName.ReadOnly = true;
            this.txtMessageFileName.Size = new System.Drawing.Size(627, 20);
            this.txtMessageFileName.TabIndex = 0;
            // 
            // txtCipherFileName
            // 
            this.txtCipherFileName.Location = new System.Drawing.Point(6, 19);
            this.txtCipherFileName.Name = "txtCipherFileName";
            this.txtCipherFileName.ReadOnly = true;
            this.txtCipherFileName.Size = new System.Drawing.Size(627, 20);
            this.txtCipherFileName.TabIndex = 1;
            // 
            // btnSetMessageFile
            // 
            this.btnSetMessageFile.Location = new System.Drawing.Point(639, 17);
            this.btnSetMessageFile.Name = "btnSetMessageFile";
            this.btnSetMessageFile.Size = new System.Drawing.Size(103, 23);
            this.btnSetMessageFile.TabIndex = 2;
            this.btnSetMessageFile.Text = "Выбрать";
            this.btnSetMessageFile.UseVisualStyleBackColor = true;
            this.btnSetMessageFile.Click += new System.EventHandler(this.btnSetMessageFile_Click);
            // 
            // btnSetCipherFile
            // 
            this.btnSetCipherFile.Location = new System.Drawing.Point(639, 17);
            this.btnSetCipherFile.Name = "btnSetCipherFile";
            this.btnSetCipherFile.Size = new System.Drawing.Size(103, 23);
            this.btnSetCipherFile.TabIndex = 3;
            this.btnSetCipherFile.Text = "Выбрать";
            this.btnSetCipherFile.UseVisualStyleBackColor = true;
            this.btnSetCipherFile.Click += new System.EventHandler(this.btnSetCipherFile_Click);
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.grpCipherFile);
            this.grpFiles.Controls.Add(this.grpMessageFile);
            this.grpFiles.Location = new System.Drawing.Point(12, 12);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(760, 126);
            this.grpFiles.TabIndex = 4;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "Файлы";
            // 
            // grpCipherFile
            // 
            this.grpCipherFile.Controls.Add(this.txtCipherFileName);
            this.grpCipherFile.Controls.Add(this.btnSetCipherFile);
            this.grpCipherFile.Location = new System.Drawing.Point(6, 73);
            this.grpCipherFile.Name = "grpCipherFile";
            this.grpCipherFile.Size = new System.Drawing.Size(748, 47);
            this.grpCipherFile.TabIndex = 1;
            this.grpCipherFile.TabStop = false;
            this.grpCipherFile.Text = "Шифр";
            // 
            // grpMessageFile
            // 
            this.grpMessageFile.Controls.Add(this.txtMessageFileName);
            this.grpMessageFile.Controls.Add(this.btnSetMessageFile);
            this.grpMessageFile.Location = new System.Drawing.Point(6, 19);
            this.grpMessageFile.Name = "grpMessageFile";
            this.grpMessageFile.Size = new System.Drawing.Size(748, 48);
            this.grpMessageFile.TabIndex = 0;
            this.grpMessageFile.TabStop = false;
            this.grpMessageFile.Text = "Сообщение";
            // 
            // btnStartCiphering
            // 
            this.btnStartCiphering.Location = new System.Drawing.Point(462, 163);
            this.btnStartCiphering.Name = "btnStartCiphering";
            this.btnStartCiphering.Size = new System.Drawing.Size(298, 23);
            this.btnStartCiphering.TabIndex = 5;
            this.btnStartCiphering.Text = "Зашифровать";
            this.btnStartCiphering.UseVisualStyleBackColor = true;
            this.btnStartCiphering.Click += new System.EventHandler(this.btnStartCiphering_Click);
            // 
            // btnStartDeciphering
            // 
            this.btnStartDeciphering.Location = new System.Drawing.Point(462, 192);
            this.btnStartDeciphering.Name = "btnStartDeciphering";
            this.btnStartDeciphering.Size = new System.Drawing.Size(298, 23);
            this.btnStartDeciphering.TabIndex = 6;
            this.btnStartDeciphering.Text = "Расшифровать";
            this.btnStartDeciphering.UseVisualStyleBackColor = true;
            this.btnStartDeciphering.Click += new System.EventHandler(this.btnStartDeciphering_Click);
            // 
            // grpKey
            // 
            this.grpKey.Controls.Add(this.gridKey);
            this.grpKey.Location = new System.Drawing.Point(12, 144);
            this.grpKey.Name = "grpKey";
            this.grpKey.Size = new System.Drawing.Size(427, 119);
            this.grpKey.TabIndex = 7;
            this.grpKey.TabStop = false;
            this.grpKey.Text = "Ключ";
            // 
            // gridKey
            // 
            this.gridKey.AllowUserToAddRows = false;
            this.gridKey.AllowUserToDeleteRows = false;
            this.gridKey.AllowUserToResizeColumns = false;
            this.gridKey.AllowUserToResizeRows = false;
            this.gridKey.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridKey.ColumnHeadersVisible = false;
            this.gridKey.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.gridKey.Location = new System.Drawing.Point(12, 19);
            this.gridKey.MultiSelect = false;
            this.gridKey.Name = "gridKey";
            this.gridKey.RowHeadersVisible = false;
            this.gridKey.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridKey.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridKey.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gridKey.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridKey.Size = new System.Drawing.Size(403, 91);
            this.gridKey.TabIndex = 0;
            this.gridKey.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridKey_CellBeginEdit);
            this.gridKey.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridKey_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSetRandomKey
            // 
            this.btnSetRandomKey.Location = new System.Drawing.Point(462, 231);
            this.btnSetRandomKey.Name = "btnSetRandomKey";
            this.btnSetRandomKey.Size = new System.Drawing.Size(298, 23);
            this.btnSetRandomKey.TabIndex = 8;
            this.btnSetRandomKey.Text = "Задать ключ случайно";
            this.btnSetRandomKey.UseVisualStyleBackColor = true;
            this.btnSetRandomKey.Click += new System.EventHandler(this.btnSetRandomKey_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 269);
            this.Controls.Add(this.btnSetRandomKey);
            this.Controls.Add(this.grpKey);
            this.Controls.Add(this.btnStartDeciphering);
            this.Controls.Add(this.btnStartCiphering);
            this.Controls.Add(this.grpFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "AES 128";
            this.grpFiles.ResumeLayout(false);
            this.grpCipherFile.ResumeLayout(false);
            this.grpCipherFile.PerformLayout();
            this.grpMessageFile.ResumeLayout(false);
            this.grpMessageFile.PerformLayout();
            this.grpKey.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridKey)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtMessageFileName;
        private System.Windows.Forms.TextBox txtCipherFileName;
        private System.Windows.Forms.Button btnSetMessageFile;
        private System.Windows.Forms.Button btnSetCipherFile;
        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.GroupBox grpCipherFile;
        private System.Windows.Forms.GroupBox grpMessageFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnStartCiphering;
        private System.Windows.Forms.Button btnStartDeciphering;
        private System.Windows.Forms.GroupBox grpKey;
        private System.Windows.Forms.DataGridView gridKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Button btnSetRandomKey;
    }
}

