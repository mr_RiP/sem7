﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    public partial class MainForm
    {
        string savedValue = null;

        private void SetFile(TextBox current, TextBox other)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (other.Text == openFileDialog.FileName)
                    ShowFileNamesWarning();
                else
                    current.Text = openFileDialog.FileName;

                openFileDialog.FileName = null;
            }
        }

        private void ShowFileNamesWarning()
        {
            MessageBox.Show("Файлы сообщения и шифра должны быть разными.",
                "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void InitializeKeyGrid()
        {
            gridKey.RowCount = 4;
            SetKeyGridValuesRandom();
        }

        private void SetKeyGridValuesRandom()
        {
            SetKeyGridValues(Aes.GenerateRandomKey(AesStandard.Aes128));
        }

        private string ByteToHexString(byte byteValue)
        {
            return byteValue.ToString("X2");
        }

        private byte HexStringToByte(string stringValue)
        {
            return Convert.ToByte(stringValue, 16);
        }

        private void SetKeyGridValues(byte[] key)
        {
            if (key.Length != 16)
                throw new ArgumentOutOfRangeException("key", "key must me byte[16] array");

            int n = 4;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    gridKey[j, i].Value = ByteToHexString(key[i * n + j]);            
        }

        private void SaveKeyGridCellValue(int columnIndex, int rowIndex)
        {
            savedValue = (string)gridKey[columnIndex, rowIndex].Value;
        }

        private void SetKeyGridCellValue(int columnIndex, int rowIndex)
        {
            try
            {
                string newValue = (string)gridKey[columnIndex, rowIndex].Value;
                byte byteValue = HexStringToByte(newValue);
                gridKey[columnIndex, rowIndex].Value = ByteToHexString(byteValue);
            }
            catch (Exception)
            {
                gridKey[columnIndex, rowIndex].Value = savedValue;
                ShowIncorrectKeyValueWarning();
            }
        }

        private void ShowIncorrectKeyValueWarning()
        {
            MessageBox.Show("Значением ключа должно быть шестнадцатеричное число от 00 до FF.\n",
                "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private byte[] GetKeyValues()
        {
            var key = new byte[16];

            int n = 4;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    key[i * n + j] = HexStringToByte((string)gridKey[j, i].Value);

            return key;
        }
    }
}
