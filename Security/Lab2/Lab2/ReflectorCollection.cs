﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    public enum GermanReflectors
    {
        ReflectorB,
        ReflectorC,
        ReflectorBDunn,
        ReflectorCDunn
    }

    /*
     
    static class ReflectorCollection
    {

        public static IReflector Get(GermanReflectors type)
        {
            switch (type)
            {
                case GermanReflectors.ReflectorB: return ReflectorB();
                case GermanReflectors.ReflectorC: return ReflectorC();
                case GermanReflectors.ReflectorBDunn: return ReflectorBDunn();
                case GermanReflectors.ReflectorCDunn: return ReflectorCDunn();
                default: throw new NotImplementedException();
            }
        }

        public static IReflector ReflectorB()
        {
            var dictionary = new Dictionary<char, char>
            {
                { 'A', 'Y' },
                { 'B', 'R' },
                { 'C', 'U' },
                { 'D', 'H' },
                { 'E', 'Q' },
                { 'F', 'S' },
                { 'G', 'L' },
                { 'I', 'P' },
                { 'J', 'X' },
                { 'K', 'N' },
                { 'M', 'O' },
                { 'T', 'Z' },
                { 'V', 'W' },
            };

            return new Reflector(FullDictionary(dictionary));
        }

        public static IReflector ReflectorC()
        {
            var dictionary = new Dictionary<char, char>
            {
                { 'A', 'F' },
                { 'B', 'V' },
                { 'C', 'P' },
                { 'D', 'J' },
                { 'E', 'I' },
                { 'G', 'O' },
                { 'H', 'Y' },
                { 'K', 'R' },
                { 'L', 'Z' },
                { 'M', 'X' },
                { 'N', 'W' },
                { 'T', 'Q' },
                { 'S', 'U' },
            };

            return new Reflector(FullDictionary(dictionary));
        }

        public static IReflector ReflectorBDunn()
        {
            var dictionary = new Dictionary<char, char>
            {
                { 'A', 'E' },
                { 'B', 'N' },
                { 'C', 'K' },
                { 'D', 'Q' },
                { 'F', 'U' },
                { 'G', 'Y' },
                { 'H', 'W' },
                { 'I', 'J' },
                { 'L', 'O' },
                { 'M', 'P' },
                { 'R', 'X' },
                { 'S', 'Z' },
                { 'T', 'V' },
            };

            return new Reflector(FullDictionary(dictionary));
        }

        public static IReflector ReflectorCDunn()
        {
            var dictionary = new Dictionary<char, char>
            {
                { 'A', 'R' },
                { 'B', 'D' },
                { 'C', 'O' },
                { 'E', 'J' },
                { 'F', 'N' },
                { 'G', 'T' },
                { 'H', 'K' },
                { 'I', 'V' },
                { 'L', 'M' },
                { 'P', 'W' },
                { 'Q', 'Z' },
                { 'S', 'X' },
                { 'U', 'Y' },
            };

            return new Reflector(FullDictionary(dictionary));
        }

        private static Dictionary<char, char> FullDictionary(Dictionary<char,char> dictionary)
        {
            var reverse = new Dictionary<char, char>();
            foreach (var kvp in dictionary)
                reverse.Add(kvp.Value, kvp.Key);
            foreach (var kvp in reverse)
                dictionary.Add(kvp.Key, kvp.Value);
            return dictionary;
        }
    }
    */
}
