﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class Rotor : IRotor
    {
        public Rotor(byte offset, List<byte> turns)
        {
            _offset = offset;
            _turns = turns;
            _pos = 0;
        }

        private byte _offset;
        private List<byte> _turns;
        private byte _pos;

        public byte Position
        {
            get
            {
                return _pos;
            }
            set
            {
                _pos = value;
            }
        }

        public byte TransformIn(byte symbolCode)
        {
            return (byte)((symbolCode + _offset) % 256);
        }

        public byte TransformOut(byte symbolCode)
        {
            if (_offset > symbolCode)
                return (byte)(symbolCode + 256 - _offset);
            else
                return (byte)(symbolCode - _offset);
        }

        public void Turn()
        {
            if (Position == byte.MaxValue)
                Position = 0;
            else
                Position++;
        }

        public bool TurnNext()
        {
            return _turns.Contains(Position);
        }
    }
}
