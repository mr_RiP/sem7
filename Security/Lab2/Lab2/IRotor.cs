﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    interface IRotor
    {
        byte Position { get; set; }
        byte TransformIn(byte symbolCode);
        byte TransformOut(byte symbolCode);
        void Turn();
        bool TurnNext();
    }
}
