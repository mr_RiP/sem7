﻿namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnSetFileA = new System.Windows.Forms.Button();
            this.btnSetFileB = new System.Windows.Forms.Button();
            this.txtFileA = new System.Windows.Forms.TextBox();
            this.txtFileB = new System.Windows.Forms.TextBox();
            this.grpParts = new System.Windows.Forms.GroupBox();
            this.cmbReflector = new System.Windows.Forms.ComboBox();
            this.cmbRightRotor = new System.Windows.Forms.ComboBox();
            this.cmbCentralRotor = new System.Windows.Forms.ComboBox();
            this.cmbLeftRotor = new System.Windows.Forms.ComboBox();
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.grpSettings = new System.Windows.Forms.GroupBox();
            this.cmbRightRotorValue = new System.Windows.Forms.ComboBox();
            this.cmbCentralRotorValue = new System.Windows.Forms.ComboBox();
            this.cmbLeftRotorValue = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.grpFileB = new System.Windows.Forms.GroupBox();
            this.grpFileA = new System.Windows.Forms.GroupBox();
            this.btnSwapFiles = new System.Windows.Forms.Button();
            this.grpParts.SuspendLayout();
            this.grpFiles.SuspendLayout();
            this.grpSettings.SuspendLayout();
            this.grpFileB.SuspendLayout();
            this.grpFileA.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSetFileA
            // 
            this.btnSetFileA.Location = new System.Drawing.Point(667, 15);
            this.btnSetFileA.Name = "btnSetFileA";
            this.btnSetFileA.Size = new System.Drawing.Size(75, 23);
            this.btnSetFileA.TabIndex = 0;
            this.btnSetFileA.Text = "Открыть";
            this.btnSetFileA.UseVisualStyleBackColor = true;
            this.btnSetFileA.Click += new System.EventHandler(this.btnSetFileA_Click);
            // 
            // btnSetFileB
            // 
            this.btnSetFileB.Location = new System.Drawing.Point(667, 18);
            this.btnSetFileB.Name = "btnSetFileB";
            this.btnSetFileB.Size = new System.Drawing.Size(75, 23);
            this.btnSetFileB.TabIndex = 1;
            this.btnSetFileB.Text = "Открыть";
            this.btnSetFileB.UseVisualStyleBackColor = true;
            this.btnSetFileB.Click += new System.EventHandler(this.btnSetFileB_Click);
            // 
            // txtFileA
            // 
            this.txtFileA.Location = new System.Drawing.Point(6, 17);
            this.txtFileA.Name = "txtFileA";
            this.txtFileA.ReadOnly = true;
            this.txtFileA.Size = new System.Drawing.Size(655, 20);
            this.txtFileA.TabIndex = 2;
            // 
            // txtFileB
            // 
            this.txtFileB.Location = new System.Drawing.Point(6, 19);
            this.txtFileB.Name = "txtFileB";
            this.txtFileB.ReadOnly = true;
            this.txtFileB.Size = new System.Drawing.Size(655, 20);
            this.txtFileB.TabIndex = 3;
            // 
            // grpParts
            // 
            this.grpParts.Controls.Add(this.cmbReflector);
            this.grpParts.Controls.Add(this.cmbRightRotor);
            this.grpParts.Controls.Add(this.cmbCentralRotor);
            this.grpParts.Controls.Add(this.cmbLeftRotor);
            this.grpParts.Location = new System.Drawing.Point(12, 149);
            this.grpParts.Name = "grpParts";
            this.grpParts.Size = new System.Drawing.Size(419, 49);
            this.grpParts.TabIndex = 4;
            this.grpParts.TabStop = false;
            this.grpParts.Text = "Компоненты машины";
            // 
            // cmbReflector
            // 
            this.cmbReflector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReflector.FormattingEnabled = true;
            this.cmbReflector.Location = new System.Drawing.Point(6, 19);
            this.cmbReflector.Name = "cmbReflector";
            this.cmbReflector.Size = new System.Drawing.Size(154, 21);
            this.cmbReflector.TabIndex = 7;
            // 
            // cmbRightRotor
            // 
            this.cmbRightRotor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRightRotor.FormattingEnabled = true;
            this.cmbRightRotor.Location = new System.Drawing.Point(166, 19);
            this.cmbRightRotor.Name = "cmbRightRotor";
            this.cmbRightRotor.Size = new System.Drawing.Size(78, 21);
            this.cmbRightRotor.TabIndex = 6;
            // 
            // cmbCentralRotor
            // 
            this.cmbCentralRotor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCentralRotor.FormattingEnabled = true;
            this.cmbCentralRotor.Location = new System.Drawing.Point(250, 19);
            this.cmbCentralRotor.Name = "cmbCentralRotor";
            this.cmbCentralRotor.Size = new System.Drawing.Size(78, 21);
            this.cmbCentralRotor.TabIndex = 5;
            // 
            // cmbLeftRotor
            // 
            this.cmbLeftRotor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLeftRotor.FormattingEnabled = true;
            this.cmbLeftRotor.Location = new System.Drawing.Point(334, 19);
            this.cmbLeftRotor.Name = "cmbLeftRotor";
            this.cmbLeftRotor.Size = new System.Drawing.Size(78, 21);
            this.cmbLeftRotor.TabIndex = 5;
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.grpFileA);
            this.grpFiles.Controls.Add(this.grpFileB);
            this.grpFiles.Location = new System.Drawing.Point(12, 12);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(760, 131);
            this.grpFiles.TabIndex = 5;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "Файлы";
            // 
            // grpSettings
            // 
            this.grpSettings.Controls.Add(this.cmbRightRotorValue);
            this.grpSettings.Controls.Add(this.cmbCentralRotorValue);
            this.grpSettings.Controls.Add(this.cmbLeftRotorValue);
            this.grpSettings.Location = new System.Drawing.Point(437, 149);
            this.grpSettings.Name = "grpSettings";
            this.grpSettings.Size = new System.Drawing.Size(163, 49);
            this.grpSettings.TabIndex = 6;
            this.grpSettings.TabStop = false;
            this.grpSettings.Text = "Настройки роторов";
            // 
            // cmbRightRotorValue
            // 
            this.cmbRightRotorValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRightRotorValue.FormattingEnabled = true;
            this.cmbRightRotorValue.Location = new System.Drawing.Point(110, 19);
            this.cmbRightRotorValue.Name = "cmbRightRotorValue";
            this.cmbRightRotorValue.Size = new System.Drawing.Size(46, 21);
            this.cmbRightRotorValue.TabIndex = 2;
            // 
            // cmbCentralRotorValue
            // 
            this.cmbCentralRotorValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCentralRotorValue.FormattingEnabled = true;
            this.cmbCentralRotorValue.Location = new System.Drawing.Point(58, 19);
            this.cmbCentralRotorValue.Name = "cmbCentralRotorValue";
            this.cmbCentralRotorValue.Size = new System.Drawing.Size(46, 21);
            this.cmbCentralRotorValue.TabIndex = 1;
            // 
            // cmbLeftRotorValue
            // 
            this.cmbLeftRotorValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLeftRotorValue.FormattingEnabled = true;
            this.cmbLeftRotorValue.Location = new System.Drawing.Point(6, 19);
            this.cmbLeftRotorValue.Name = "cmbLeftRotorValue";
            this.cmbLeftRotorValue.Size = new System.Drawing.Size(46, 21);
            this.cmbLeftRotorValue.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(606, 149);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(166, 23);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "Выполнить шифрование";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // grpFileB
            // 
            this.grpFileB.Controls.Add(this.btnSetFileB);
            this.grpFileB.Controls.Add(this.txtFileB);
            this.grpFileB.Location = new System.Drawing.Point(6, 71);
            this.grpFileB.Name = "grpFileB";
            this.grpFileB.Size = new System.Drawing.Size(748, 50);
            this.grpFileB.TabIndex = 4;
            this.grpFileB.TabStop = false;
            this.grpFileB.Text = "Шифр";
            // 
            // grpFileA
            // 
            this.grpFileA.Controls.Add(this.btnSetFileA);
            this.grpFileA.Controls.Add(this.txtFileA);
            this.grpFileA.Location = new System.Drawing.Point(6, 19);
            this.grpFileA.Name = "grpFileA";
            this.grpFileA.Size = new System.Drawing.Size(748, 46);
            this.grpFileA.TabIndex = 5;
            this.grpFileA.TabStop = false;
            this.grpFileA.Text = "Сообщение";
            // 
            // btnSwapFiles
            // 
            this.btnSwapFiles.Location = new System.Drawing.Point(606, 178);
            this.btnSwapFiles.Name = "btnSwapFiles";
            this.btnSwapFiles.Size = new System.Drawing.Size(166, 23);
            this.btnSwapFiles.TabIndex = 8;
            this.btnSwapFiles.Text = "Поменять файлы местами";
            this.btnSwapFiles.UseVisualStyleBackColor = true;
            this.btnSwapFiles.Click += new System.EventHandler(this.btnSwapFiles_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 206);
            this.Controls.Add(this.btnSwapFiles);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.grpSettings);
            this.Controls.Add(this.grpFiles);
            this.Controls.Add(this.grpParts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Энигма";
            this.grpParts.ResumeLayout(false);
            this.grpFiles.ResumeLayout(false);
            this.grpSettings.ResumeLayout(false);
            this.grpFileB.ResumeLayout(false);
            this.grpFileB.PerformLayout();
            this.grpFileA.ResumeLayout(false);
            this.grpFileA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnSetFileA;
        private System.Windows.Forms.Button btnSetFileB;
        private System.Windows.Forms.TextBox txtFileA;
        private System.Windows.Forms.TextBox txtFileB;
        private System.Windows.Forms.GroupBox grpParts;
        private System.Windows.Forms.ComboBox cmbLeftRotor;
        private System.Windows.Forms.ComboBox cmbCentralRotor;
        private System.Windows.Forms.ComboBox cmbReflector;
        private System.Windows.Forms.ComboBox cmbRightRotor;
        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.GroupBox grpSettings;
        private System.Windows.Forms.ComboBox cmbRightRotorValue;
        private System.Windows.Forms.ComboBox cmbCentralRotorValue;
        private System.Windows.Forms.ComboBox cmbLeftRotorValue;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox grpFileA;
        private System.Windows.Forms.GroupBox grpFileB;
        private System.Windows.Forms.Button btnSwapFiles;
    }
}

