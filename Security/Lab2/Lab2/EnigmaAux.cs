﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    static class EnigmaAux
    {
        public static Dictionary<char,char> DictionaryCorrector(Dictionary<char,char> dict)
        {
            const int LEN = 26;

            if (dict.Count != LEN)
                throw new ArgumentException("Dictionary must have " + LEN.ToString() + " elements");

            var res = new Dictionary<char, char>();
            foreach (var kvp in dict)
            {
                char key = LetterCorrector(kvp.Key);
                char value = LetterCorrector(kvp.Value);

                if (res.ContainsKey(key) || res.ContainsValue(value))
                    throw new ArgumentException("Each key and each value must be unique for its part");
                else
                    res.Add(key, value);
            }

            return res;
        }

        public static List<string> CyclesCorrector(List<string> cycles)
        {
            var res = new List<string>();
            foreach (string str in cycles)
            {
                var upper = str.ToUpper();
                foreach (var letter in upper)
                {
                    if ('A' > letter || letter > 'Z')
                        throw new ArgumentException("Each char in cycle strings must be a latin letter");
                }
                res.Add(upper);
            }

            return res;
        }

        public static List<char> ListCorrector(List<char> letters)
        {
            var res = new List<char>();
            var len = letters.Count;
            for (int i = 0; i < len; ++i)
                res.Add(LetterCorrector(letters[i]));
            return res;
        }

        public static char LetterCorrector(char letter)
        {
            if ('a' <= letter && letter <= 'z')
               letter = (char)(letter - 'a' + 'A');
            if ('A' <= letter && letter <= 'Z')
                return letter;
            else
                throw new ArgumentException("Each char must be a latin letter");
        }

        public static Dictionary<char, char> DictionaryFromCycles(List<string> cycles)
        {
            var res = new Dictionary<char, char>();
            foreach(string cycle in cycles)
            {
                int n = cycle.Length - 1;
                for (int i = 0; i < n; ++i)
                    res.Add(cycle[i], cycle[i + 1]);
                res.Add(cycle[n], cycle[0]);
            }
            return res;
        }

        public static char[] GetAlphabet()
        {
            int len = 'Z' - 'A' + 1;
            var alphabet = new char[len];
            for (int i = 0; i < len; i++)
                alphabet[i] = (char)('A' + i);

            return alphabet;
        }
    }
}
