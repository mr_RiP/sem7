﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    interface IReflector
    {
        byte Transform(byte symbolCode);
    }
}
