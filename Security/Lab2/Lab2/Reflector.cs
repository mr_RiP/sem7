﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class Reflector : IReflector
    {
        public Reflector(Dictionary<byte,byte> dictionary)
        {
            _dictionary = dictionary;
        }

        public Reflector()
        {
            _dictionary = new Dictionary<byte, byte>();
            byte offset = 128;
            for (byte i = 0; i < offset; ++i)
            {
                byte j = (byte)(i + offset);
                _dictionary.Add(i, j);
                _dictionary.Add(j, i);
            }
        }

        private Dictionary<byte, byte> _dictionary;

        public byte Transform(byte symbolCode)
        {
            return _dictionary[symbolCode];
        }
    }
}
