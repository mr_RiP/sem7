﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    static class ComboBoxItems
    {
        public class GermanReflectorItem
        {
            public string Name { get; set; }
            public GermanReflectors Type { get; set; }
        }

        public static GermanReflectorItem[] GermanReflectorItems()
        {
            return new GermanReflectorItem[]
            {
                new GermanReflectorItem { Name = "Reflector B",         Type = GermanReflectors.ReflectorB },
                new GermanReflectorItem { Name = "Reflector C",         Type = GermanReflectors.ReflectorC },
                new GermanReflectorItem { Name = "Reflector B Dünn",    Type = GermanReflectors.ReflectorBDunn },
                new GermanReflectorItem { Name = "Reflector C Dünn",    Type = GermanReflectors.ReflectorCDunn },
            };
        }

        public class GermanRotorItem
        {
            public string Name { get; set; }
            public GermanRotors Type { get; set; }
        }

        public static GermanRotorItem[] GermanRotorItems()
        {
            return new GermanRotorItem[]
            {
                new GermanRotorItem { Name = "Rotor I",     Type = GermanRotors.RotorI },
                new GermanRotorItem { Name = "Rotor II",    Type = GermanRotors.RotorII },
                new GermanRotorItem { Name = "Rotor III",   Type = GermanRotors.RotorIII },
                new GermanRotorItem { Name = "Rotor IV",    Type = GermanRotors.RotorIV },
                new GermanRotorItem { Name = "Rotor V",     Type = GermanRotors.RotorV },
                new GermanRotorItem { Name = "Rotor VI",    Type = GermanRotors.RotorVI },
                new GermanRotorItem { Name = "Rotor VII",   Type = GermanRotors.RotorVII },
                new GermanRotorItem { Name = "Rotor VIII",  Type = GermanRotors.RotorVIII },
            };
        }
    }
}
