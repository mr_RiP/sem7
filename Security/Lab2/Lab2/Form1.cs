﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        private int leftRotorIndex;
        private int centralRotorIndex;
        private int rightRotorIndex;

        public Form1()
        {
            InitializeComponent();
            InitializeParts();
            InitializeSettings();
        }

        private byte[] GetBytes()
        {
            var bytes = new byte[256];
            for (int i = 0; i < bytes.Length; ++i)
                bytes[i] = (byte)i;

            return bytes;
        }

        private void InitializeSettings()
        {
            cmbLeftRotorValue.DataSource = GetBytes();
            cmbCentralRotorValue.DataSource = GetBytes();
            cmbRightRotorValue.DataSource = GetBytes();
        }

        private void InitializeParts()
        {
            cmbReflector.DataSource = ComboBoxItems.GermanReflectorItems();
            cmbReflector.DisplayMember = "Name";
            cmbReflector.ValueMember = "Type";

            cmbLeftRotor.DataSource = ComboBoxItems.GermanRotorItems();
            cmbCentralRotor.DataSource = ComboBoxItems.GermanRotorItems();
            cmbRightRotor.DataSource = ComboBoxItems.GermanRotorItems();

            cmbLeftRotor.DisplayMember = "Name";
            cmbLeftRotor.ValueMember = "Type";

            cmbCentralRotor.DisplayMember = "Name";
            cmbCentralRotor.ValueMember = "Type";

            cmbRightRotor.DisplayMember = "Name";
            cmbRightRotor.ValueMember = "Type";

            cmbLeftRotor.SelectedIndex = leftRotorIndex = 0;
            cmbCentralRotor.SelectedIndex = centralRotorIndex = 1;
            cmbRightRotor.SelectedIndex = rightRotorIndex = 2;

            cmbLeftRotor.SelectedIndexChanged += new EventHandler(cmbLeftRotor_SelectedIndexChanged);
            cmbCentralRotor.SelectedIndexChanged += new EventHandler(cmbCentralRotor_SelectedIndexChanged);
            cmbRightRotor.SelectedIndexChanged += new EventHandler(cmbRightRotor_SelectedIndexChanged);
        }

        private void btnSetFileA_Click(object sender, EventArgs e)
        {
            SetFileName(txtFileA, txtFileB);
        }

        private void btnSetFileB_Click(object sender, EventArgs e)
        {
            SetFileName(txtFileB, txtFileA);
        }

        private void SetFileName(TextBox currentFileText, TextBox otherFileTextBox)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(otherFileTextBox.Text) && otherFileTextBox.Text == openFileDialog.FileName)
                    FilesMustBeDifferentWarning();
                else
                {
                    currentFileText.Text = openFileDialog.FileName;
                    openFileDialog.FileName = null;
                }
            }
        }

        private void FilesMustBeDifferentWarning()
        {
            MessageBox.Show("Задаваемые файлы должны быть разными", "Предупреждение",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void cmbLeftRotor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLeftRotor.SelectedIndex == cmbCentralRotor.SelectedIndex ||
                cmbLeftRotor.SelectedIndex == cmbRightRotor.SelectedIndex)
            {
                ShowRotorSelectionWarning();
                cmbLeftRotor.SelectedIndex = leftRotorIndex;
            }
            else
                leftRotorIndex = cmbLeftRotor.SelectedIndex;
        }

        private void cmbCentralRotor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCentralRotor.SelectedIndex == cmbLeftRotor.SelectedIndex ||
                cmbCentralRotor.SelectedIndex == cmbRightRotor.SelectedIndex)
            {
                ShowRotorSelectionWarning();
                cmbCentralRotor.SelectedIndex = centralRotorIndex;
            }
            else
                centralRotorIndex = cmbCentralRotor.SelectedIndex;
        }

        private void cmbRightRotor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRightRotor.SelectedIndex == cmbCentralRotor.SelectedIndex ||
                cmbRightRotor.SelectedIndex == cmbLeftRotor.SelectedIndex)
            {
                ShowRotorSelectionWarning();
                cmbRightRotor.SelectedIndex = rightRotorIndex;
            }
            else
                rightRotorIndex = cmbRightRotor.SelectedIndex;
        }

        private void ShowRotorSelectionWarning()
        {
            MessageBox.Show("Выбранные роторы не должны дублироваться", "Предупреждение",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (FilesAreSet())
            {
                try
                {
                    var machine = SetupEnigma();
                    CipherBinaryFiles(machine, txtFileA.Text, txtFileB.Text);
                    ShowCurrentState(machine);
                    ShowSuccessMessage();
                }
                catch (Exception error)
                {
                    MessageBox.Show("При выполнении операции произошла ошибка:\n" + error.Message, "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                FilesNotSetWarning();
        }

        private void ShowSuccessMessage()
        {
            MessageBox.Show("Операция успешно выполнена", "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FilesNotSetWarning()
        {
            MessageBox.Show("Для выполнения операции необходимо задать файлы", "Ошибка",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private bool FilesAreSet()
        {
            return !string.IsNullOrEmpty(txtFileA.Text) && !string.IsNullOrEmpty(txtFileB.Text);
        }

        private void ShowCurrentState(Enigma machine)
        {
            cmbRightRotorValue.SelectedIndex = machine.RightRing;
            cmbCentralRotorValue.SelectedIndex = machine.CentralRing;
            cmbLeftRotorValue.SelectedIndex = machine.LeftRing;
        }

        private void CipherBinaryFiles(Enigma machine, string inFileName, string outFileName)
        {
            Enigma enigma = SetupEnigma();
            byte[] bytes = File.ReadAllBytes(inFileName);
            for (int i = 0; i < bytes.Length; ++i)
                bytes[i] = enigma.Transform(bytes[i]);
            File.WriteAllBytes(outFileName, bytes);
        }

        private Enigma SetupEnigma()
        {
            var machine = new Enigma();

            machine.RightRing = (byte)cmbRightRotorValue.SelectedItem;
            machine.CentralRing = (byte)cmbCentralRotorValue.SelectedItem;
            machine.LeftRing = (byte)cmbLeftRotorValue.SelectedItem;

            return machine;
        }

        private void btnSwapFiles_Click(object sender, EventArgs e)
        {
            string swap = txtFileA.Text;
            txtFileA.Text = txtFileB.Text;
            txtFileB.Text = swap;
        }
    }
}
