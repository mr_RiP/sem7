﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    class Enigma
    {
        public Enigma()
        {
            _rightRotor = new Rotor(7, new List<byte> { 0 });
            _centralRotor = new Rotor(13, new List<byte> { 10 });
            _leftRotor = new Rotor(3, new List<byte> { 15 });
            _reflector = new Reflector();
        }

        public byte LeftRing
        {
            get
            {
                return _leftRotor.Position;
            }

            set
            {
                _leftRotor.Position = value;
            }
        }

        public byte CentralRing
        {
            get
            {
                return _centralRotor.Position;
            }

            set
            {
                _centralRotor.Position = value;
            }
        }

        public byte RightRing
        {
            get
            {
                return _rightRotor.Position;
            }

            set
            {
                _rightRotor.Position = value;
            }
        }

        private IRotor _rightRotor;
        private IRotor _centralRotor;
        private IRotor _leftRotor;
        private IReflector _reflector;

        public byte Transform(byte symbol)
        {
            TurnRings();
            return InnerTransform(symbol);
        }

        private byte Reflect(byte symbol)
        {
            return symbol;
        }

        private byte InnerTransform(byte symbol)
        {
            int symbolCode = symbol;

            int leftCode = _leftRotor.Position;
            int centralCode = _centralRotor.Position;
            int rightCode = _rightRotor.Position;

            int rightDiff = Substraction(centralCode, rightCode);
            int leftDiff = Substraction(leftCode, centralCode);

            symbolCode = Addition(symbolCode, rightCode);
            symbolCode = _rightRotor.TransformIn((byte)symbolCode);

            symbolCode = Addition(symbolCode, rightDiff);
            symbolCode = _centralRotor.TransformIn((byte)symbolCode);

            symbolCode = Addition(symbolCode, leftDiff);
            symbolCode = _leftRotor.TransformIn((byte)symbolCode);

            symbolCode = Substraction(symbolCode, leftCode);
            symbolCode = _reflector.Transform((byte)symbolCode);
            symbolCode = Addition(symbolCode, leftCode);

            symbolCode = _leftRotor.TransformOut((byte)symbolCode);
            symbolCode = Substraction(symbolCode, leftDiff);

            symbolCode = _centralRotor.TransformOut((byte)symbolCode);
            symbolCode = Substraction(symbolCode, rightDiff);

            symbolCode = _rightRotor.TransformOut((byte)symbolCode);
            symbolCode = Substraction(symbolCode, rightCode);

            symbol = (byte)symbolCode;
            return symbol;
        }

        private void TurnRings()
        {
            _rightRotor.Turn();
            if (_rightRotor.TurnNext())
            {
                _centralRotor.Turn();
                if (_centralRotor.TurnNext())
                    _leftRotor.Turn();
            }
        }

        private int Substraction(int a, int b)
        {
            return a < b ? a + 256 - b : a - b;
        }

        private int Addition(int a, int b)
        {
            return (a + b) % 256;
        }
    }
}
