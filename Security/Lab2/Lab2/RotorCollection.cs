﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
    public enum GermanRotors
    {
        RotorI,
        RotorII,
        RotorIII,
        RotorIV,
        RotorV,
        RotorVI,
        RotorVII,
        RotorVIII
    }
    /*
    static class RotorCollection
    {
        public static IRotor Get(GermanRotors type)
        {
            switch (type)
            {
                case GermanRotors.RotorI: return RotorI();
                case GermanRotors.RotorII: return RotorII();
                case GermanRotors.RotorIII: return RotorIII();
                case GermanRotors.RotorIV: return RotorIV();
                case GermanRotors.RotorV: return RotorV();
                case GermanRotors.RotorVI: return RotorVI();
                case GermanRotors.RotorVII: return RotorVII();
                case GermanRotors.RotorVIII: return RotorVIII();
                default: throw new NotImplementedException();
            }
        }

        public static IRotor RotorI()
        {
            var cycles = new List<string>
            {
                "AELTPHQXRU", "BKNW", "CMOY", "DFG", "IV", "JZ", "S"
            };
            var turns = new List<char>
            {
                'R'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorII()
        {
            var cycles = new List<string>
            {
                "FIXVYOMW", "CDKLHUP", "ESZ", "BJ", "GR", "NT", "A", "Q"
            };
            var turns = new List<char>
            {
                'F'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorIII()
        {
            var cycles = new List<string>
            {
                "ABDHPEJT", "CFLVMZOYQIRWUKXSG", "N"
            };
            var turns = new List<char>
            {
                'W'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorIV()
        {
            var cycles = new List<string>
            {
                "AEPLIYWCOXMRFZBSTGJQNH", "DV", "KU"
            };
            var turns = new List<char>
            {
                'K'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorV()
        {
            var cycles = new List<string>
            {
                "AVOLDRWFIUQ", "BZKSMNHYC", "EGTJPX"
            };
            var turns = new List<char>
            {
                'A'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorVI()
        {
            var cycles = new List<string>
            {
                "AJQDVLEOZWIYTS", "CGMNHFUX", "BPRK"
            };
            var turns = new List<char>
            {
                'A', 'N'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorVII()
        {
            var cycles = new List<string>
            {
                "ANOUPFRIMBZTLWKSVEGCJYDHXQ"
            };
            var turns = new List<char>
            {
                'A', 'N'
            };

            return new Rotor(cycles, turns);
        }

        public static IRotor RotorVIII()
        {
            var cycles = new List<string>
            {
                "AFLSETWUNDHOZVICQ", "BKJ", "GXY", "MPR"
            };
            var turns = new List<char>
            {
                'A', 'N'
            };

            return new Rotor(cycles, turns);
        }
    }
    */
}
