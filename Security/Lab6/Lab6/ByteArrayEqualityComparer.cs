﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    using System;
    using System.Collections.Generic;

    public sealed class ByteArrayEqualityComparer: IEqualityComparer<byte[]>
    {
        public bool Equals(byte[] a, byte[] b)
        {
            if (a == b)
                return true;

            if (a == null || b == null)
                return false;

            if (a.Length != b.Length)
                return false;

            for (int i = 0; i < a.Length; ++i)
                if (a[i] != b[i])
                    return false;

            return true;
        }

        public int GetHashCode(byte[] array)
        {
            unchecked
            {
                if (array == null)
                    return 0;

                int hash = 17;
                foreach (byte element in array)
                    hash = hash * 31 + element.GetHashCode();

                return hash;
            }
        }
    }
}
