﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public partial class MainForm
    {
        private void SetFile(TextBox current, TextBox other)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (other.Text == openFileDialog.FileName)
                    ShowFileNamesWarning();
                else
                    current.Text = openFileDialog.FileName;

                openFileDialog.FileName = null;
            }
        }

        private void ShowFileNamesWarning()
        {
            MessageBox.Show("Файлы сообщения и шифра должны быть разными.\nБыло восстановлено прежнее значение.",
                "Преудпреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void ProcessFiles(string inputFile, string outputFile, Func<byte[], byte[]> func)
        {
            if (CheckFiles())
            {
                try
                {
                    byte[] bytes = File.ReadAllBytes(inputFile);
                    bytes = func(bytes);
                    File.WriteAllBytes(outputFile, bytes);
				
                    MessageBox.Show("Операция успешно завершена", "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Во время выполнения операции произошла ошибка:\n" + e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
				
            }
        }

        private bool CheckFiles()
        {
            bool cipherNull = string.IsNullOrEmpty(txtCipherFileName.Text);
            bool messageNull = string.IsNullOrEmpty(txtMessageFileName.Text);

            if (cipherNull && messageNull)
            {
                MessageBox.Show("Не указаны файлы для обработки", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (cipherNull)
            {
                MessageBox.Show("Не указан файл шифра", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (messageNull)
            {
                MessageBox.Show("Не указан файл сообщения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
