﻿namespace Lab6
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.grpCipherFile = new System.Windows.Forms.GroupBox();
            this.txtCipherFileName = new System.Windows.Forms.TextBox();
            this.btnOpenCipherFile = new System.Windows.Forms.Button();
            this.grpMessageFile = new System.Windows.Forms.GroupBox();
            this.txtMessageFileName = new System.Windows.Forms.TextBox();
            this.btnOpenMessageFile = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnEncode = new System.Windows.Forms.Button();
            this.btnDecode = new System.Windows.Forms.Button();
            this.grpFiles.SuspendLayout();
            this.grpCipherFile.SuspendLayout();
            this.grpMessageFile.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.grpCipherFile);
            this.grpFiles.Controls.Add(this.grpMessageFile);
            this.grpFiles.Location = new System.Drawing.Point(12, 12);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(760, 125);
            this.grpFiles.TabIndex = 1;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "Файлы";
            // 
            // grpCipherFile
            // 
            this.grpCipherFile.Controls.Add(this.txtCipherFileName);
            this.grpCipherFile.Controls.Add(this.btnOpenCipherFile);
            this.grpCipherFile.Location = new System.Drawing.Point(6, 72);
            this.grpCipherFile.Name = "grpCipherFile";
            this.grpCipherFile.Size = new System.Drawing.Size(748, 48);
            this.grpCipherFile.TabIndex = 1;
            this.grpCipherFile.TabStop = false;
            this.grpCipherFile.Text = "Шифр";
            // 
            // txtCipherFileName
            // 
            this.txtCipherFileName.Location = new System.Drawing.Point(6, 19);
            this.txtCipherFileName.Name = "txtCipherFileName";
            this.txtCipherFileName.ReadOnly = true;
            this.txtCipherFileName.Size = new System.Drawing.Size(655, 20);
            this.txtCipherFileName.TabIndex = 3;
            // 
            // btnOpenCipherFile
            // 
            this.btnOpenCipherFile.Location = new System.Drawing.Point(667, 17);
            this.btnOpenCipherFile.Name = "btnOpenCipherFile";
            this.btnOpenCipherFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenCipherFile.TabIndex = 2;
            this.btnOpenCipherFile.Text = "Открыть";
            this.btnOpenCipherFile.UseVisualStyleBackColor = true;
            this.btnOpenCipherFile.Click += new System.EventHandler(this.btnOpenCipherFile_Click);
            // 
            // grpMessageFile
            // 
            this.grpMessageFile.Controls.Add(this.txtMessageFileName);
            this.grpMessageFile.Controls.Add(this.btnOpenMessageFile);
            this.grpMessageFile.Location = new System.Drawing.Point(6, 19);
            this.grpMessageFile.Name = "grpMessageFile";
            this.grpMessageFile.Size = new System.Drawing.Size(748, 47);
            this.grpMessageFile.TabIndex = 0;
            this.grpMessageFile.TabStop = false;
            this.grpMessageFile.Text = "Сообщение";
            // 
            // txtMessageFileName
            // 
            this.txtMessageFileName.Location = new System.Drawing.Point(6, 19);
            this.txtMessageFileName.Name = "txtMessageFileName";
            this.txtMessageFileName.ReadOnly = true;
            this.txtMessageFileName.Size = new System.Drawing.Size(655, 20);
            this.txtMessageFileName.TabIndex = 1;
            // 
            // btnOpenMessageFile
            // 
            this.btnOpenMessageFile.Location = new System.Drawing.Point(667, 17);
            this.btnOpenMessageFile.Name = "btnOpenMessageFile";
            this.btnOpenMessageFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenMessageFile.TabIndex = 0;
            this.btnOpenMessageFile.Text = "Открыть";
            this.btnOpenMessageFile.UseVisualStyleBackColor = true;
            this.btnOpenMessageFile.Click += new System.EventHandler(this.btnOpenMessageFile_Click);
            // 
            // btnEncode
            // 
            this.btnEncode.Location = new System.Drawing.Point(506, 143);
            this.btnEncode.Name = "btnEncode";
            this.btnEncode.Size = new System.Drawing.Size(130, 23);
            this.btnEncode.TabIndex = 2;
            this.btnEncode.Text = "Кодировать";
            this.btnEncode.UseVisualStyleBackColor = true;
            this.btnEncode.Click += new System.EventHandler(this.btnEncode_Click);
            // 
            // btnDecode
            // 
            this.btnDecode.Location = new System.Drawing.Point(642, 143);
            this.btnDecode.Name = "btnDecode";
            this.btnDecode.Size = new System.Drawing.Size(130, 23);
            this.btnDecode.TabIndex = 3;
            this.btnDecode.Text = "Раскодировать";
            this.btnDecode.UseVisualStyleBackColor = true;
            this.btnDecode.Click += new System.EventHandler(this.btnDecode_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 174);
            this.Controls.Add(this.btnDecode);
            this.Controls.Add(this.btnEncode);
            this.Controls.Add(this.grpFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "LZW";
            this.grpFiles.ResumeLayout(false);
            this.grpCipherFile.ResumeLayout(false);
            this.grpCipherFile.PerformLayout();
            this.grpMessageFile.ResumeLayout(false);
            this.grpMessageFile.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.GroupBox grpCipherFile;
        private System.Windows.Forms.TextBox txtCipherFileName;
        private System.Windows.Forms.Button btnOpenCipherFile;
        private System.Windows.Forms.GroupBox grpMessageFile;
        private System.Windows.Forms.TextBox txtMessageFileName;
        private System.Windows.Forms.Button btnOpenMessageFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnEncode;
        private System.Windows.Forms.Button btnDecode;
    }
}

