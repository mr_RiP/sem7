﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnEncode_Click(object sender, EventArgs e)
        {
            ProcessFiles(txtMessageFileName.Text, txtCipherFileName.Text, Lzw.Encode);
        }

        private void btnDecode_Click(object sender, EventArgs e)
        {
            ProcessFiles(txtCipherFileName.Text, txtMessageFileName.Text, Lzw.Decode);
        }

        private void btnOpenMessageFile_Click(object sender, EventArgs e)
        {
            SetFile(txtMessageFileName, txtCipherFileName);
        }

        private void btnOpenCipherFile_Click(object sender, EventArgs e)
        {
            SetFile(txtCipherFileName, txtMessageFileName);
        }
    }
}