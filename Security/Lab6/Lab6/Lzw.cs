﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    public static class Lzw
    {
        public static byte[] Encode(byte[] message)
        {
            if (message == null)
                throw new ArgumentNullException();

            if (message.Length == 0)
                throw new ArgumentException("Cообщение пусто.");


            Dictionary<byte[], int> dictionary = InitializeEncodeDictionary();
            var result = new List<int>();
            var inputPhrase = new List<byte>();

            inputPhrase.Add(message[0]);
            for (int i = 1; i < message.Length; ++i)
            {
                byte[] phrase = BuildPhrase(inputPhrase, message[i]);
                if (dictionary.ContainsKey(phrase))
                    inputPhrase.Add(message[i]);
                else
                {
                    result.Add(dictionary[inputPhrase.ToArray()]);
                    dictionary.Add(phrase, dictionary.Count);
                    inputPhrase = new List<byte> { message[i] };
                }
            }
            result.Add(dictionary[inputPhrase.ToArray()]);

            return GetBytesArray(result);
        }

        public static byte[] Decode(byte[] encodedMessage)
        {
            if (encodedMessage == null)
                throw new ArgumentNullException();

            if (encodedMessage.Length == 0)
                throw new ArgumentException("Закодированное сообщение пусто.");

            Dictionary<int, byte[]> dictionary = InitializeDecodeDictionary();
            List<byte> result = new List<byte>();
            int[] codes = GetCodesArray(encodedMessage);
            var inputPhrase = new List<byte>();

            if (!dictionary.ContainsKey(codes[0]))
                throw new ArgumentException("Закодированное сообщение начинается с неизвестного кода.");

            inputPhrase.AddRange(dictionary[codes[0]]);
            for (int i = 1; i < codes.Length; ++i)
            {
                if (dictionary.ContainsKey(codes[i]))
                {
                    result.AddRange(inputPhrase);
                    dictionary.Add(dictionary.Count, BuildPhrase(inputPhrase, dictionary[codes[i]][0]));
                    inputPhrase = new List<byte>(dictionary[codes[i]]);
                }
                else
                {
                    result.AddRange(inputPhrase);
                    dictionary.Add(dictionary.Count, BuildPhrase(inputPhrase, inputPhrase[0]));
                    inputPhrase = new List<byte>(dictionary[codes[i]]);
                }
            }
            result.AddRange(inputPhrase);

            return result.ToArray();
        }

        private static int[] GetCodesArray(byte[] encodedMessage)
        {
            int codeSize = sizeof(int);
            if (encodedMessage.Length % codeSize != 0)
                throw new ArgumentException("Байты закодированного сообщения невозможно корректно разбить на коды.");

            var list = new List<int>();
            for (int i = 0; i < encodedMessage.Length; i += codeSize)
                list.Add(BitConverter.ToInt32(encodedMessage, i));

            return list.ToArray();
        }

        private static Dictionary<byte[], int> InitializeEncodeDictionary()
        {
            var dictionary = new Dictionary<byte[], int>(new ByteArrayEqualityComparer());

            for (int i = byte.MinValue; i <= byte.MaxValue; ++i)
                dictionary.Add(new byte[] { (byte)i }, i);

            return dictionary;
        }

        private static byte[] BuildPhrase(List<byte> inputPhrase, byte symbol)
        {
            var phrase = new List<byte>(inputPhrase);
            phrase.Add(symbol);
            return phrase.ToArray();
        }

        private static byte[] GetBytesArray(List<int> list)
        {
            int byteCount = sizeof(int);
            var array = new byte[list.Count * byteCount];
            for (int i = 0; i < list.Count; ++i)
                BitConverter.GetBytes(list[i]).CopyTo(array, i * byteCount);

            return array;
        }

        private static Dictionary<int, byte[]> InitializeDecodeDictionary()
        {
            var dictionary = new Dictionary<int, byte[]>();

            for (int i = byte.MinValue; i <= byte.MaxValue; ++i)
                dictionary.Add(i, new byte[] { (byte)i });

            return dictionary;
        }
    }
}
