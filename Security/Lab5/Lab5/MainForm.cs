﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeCipherer();
        }

        private void btnOpenMessageFile_Click(object sender, EventArgs e)
        {
            SetFile(txtMessageFileName, txtSignFileName);
        }

        private void btnOpenSignFile_Click(object sender, EventArgs e)
        {
            SetFile(txtSignFileName, txtMessageFileName);
        }

        private void btnGenerateKeys_Click(object sender, EventArgs e)
        {
            GenerateRandomSettings();
        }

        private void btnCreateSign_Click(object sender, EventArgs e)
        {
            CreateSign();
        }

        private void btnCompareSign_Click(object sender, EventArgs e)
        {
            CompareSign();
        }

        private void btnChangeKey_Click(object sender, EventArgs e)
        {
            ToggleManualMode();
        }
    }
}
