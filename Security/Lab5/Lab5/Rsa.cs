﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Lab5
{
    public enum RsaDefaultExponent
    {
        Fermat3 = 3,
        Fermat5 = 5, 
        Fermat17 = 17, 
        Fermat257 = 257, 
        Fermat65537 = 65537
    }

    public class Rsa
    {
        public Rsa(int q, int p, int e)
        {
            Q = q;
            P = p;
            E = e;

            GenerateKeys();
        }

        public void GenerateKeys()
        {
            N = BigInteger.Multiply(p, q);
            var euler = BigInteger.Multiply(q - 1, p - 1);
            D = Inverse(e, euler);
        }

        private static BigInteger Inverse(BigInteger a, BigInteger n)
        {
            BigInteger t = 0;
            BigInteger r = n;

            BigInteger newT = 1;
            BigInteger newR = a;

            BigInteger temp;

            while (newR != 0)
            {
                BigInteger quotient = r / newR;

                temp = t;
                t = newT;
                newT = temp - quotient * newT;

                temp = r;
                r = newR;
                newR = temp - quotient * newR;

            }

            if (r > BigInteger.One)
                throw new ArgumentException("A is not invertible.");
            if (t < BigInteger.Zero)
                t += n;

            return t;
        }

        public void SetDefaultE(RsaDefaultExponent defaultValue)
        {
            E = (int)defaultValue;
        }

        public byte[] Encipher(byte[] message)
        {
            return Encipher(message, e);
        }

        public byte[] Decipher(byte[] cipher)
        {
            return Decipher(cipher, d);
        }

        public byte[] CreateSign(byte[] message)
        {
            byte[] hash = GenerateHash(message);
            return Encipher(hash, d);
        }

        private byte[] GenerateHash(byte[] message)
        {
            var hasher = new SHA1CryptoServiceProvider();
            return hasher.ComputeHash(message);
        }

        public bool CompareSign(byte[] message, byte[] sign)
        {
            byte[] signHash = Decipher(sign, e);
            byte[] messageHash = GenerateHash(message);
            return CompareBytes(messageHash, signHash);
        }

        private bool CompareBytes(byte[] a, byte[] b)
        {
            if (a == null || b == null)
                return false;
            if (a.Length != b.Length)
                return false;
            for (int i = 0; i < a.Length; ++i)
                if (a[i] != b[i])
                    return false;

            return true;
        }

        private byte[] Encipher(byte[] message, BigInteger key)
        {
            var cipher = new byte[message.Length * byteCount];
            for (int i = 0; i < message.Length; ++i)
            { 
                byte[] cipherPart = BigInteger.ModPow(message[i], key, n).ToByteArray();
                int partBeginIndex = i * byteCount;
                for (int j = 0; j < cipherPart.Length; ++j)
                    cipher[partBeginIndex + j] = cipherPart[j];
            }

            return cipher;
        }

        private byte[] Decipher(byte[] cipher, BigInteger key)
        {
            var message = new byte[cipher.Length / byteCount];
            var cipherPart = new byte[byteCount];
            for (int i = 0; i < message.Length; ++i)
            {
                Array.Copy(cipher, i * byteCount, cipherPart, 0, byteCount);
                message[i] = (byte)BigInteger.ModPow(new BigInteger(cipherPart), key, n);
            }

            return message;
        }

        public int E
        {
            get
            {
                return e;
            }
            set
            {
                if (value <= 0)
                    throw new AggregateException("E must be positive.");

                e = value;
            }
        }

        public int P
        {
            get
            {
                return p;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("P must be positive value.");

                if (value == q)
                    throw new ArgumentException("P and Q must have different values.");

                p = value;
            }
        }

        public int Q
        {
            get
            {
                return q;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("Q must be positive value.");

                if (value == p)
                    throw new ArgumentException("P and Q must have different values.");

                 q = value;
            }
        }

        public BigInteger N
        {
            get
            {
                return n;
            }
            set
            {
                if (value.Sign <= 0)
                    throw new ArgumentException("N must be positive value.");

                n = value;

                byteCount = n.ToByteArray().Length;
            }
        }

        public BigInteger D
        {
            get
            {
                return d;
            }
            set
            {
                if (value.Sign <= 0)
                    throw new ArgumentException("D must be positive value.");

                d = value;
            }
        }

        private int p;
        private int q;
        private int e;
        private BigInteger n;
        private BigInteger d;
        private int byteCount;
    }
}
