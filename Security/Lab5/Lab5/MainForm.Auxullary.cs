﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Lab5
{
    public partial class MainForm
    {
        private bool manualMode;

        private void SetFile(TextBox current, TextBox other)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (other.Text == openFileDialog.FileName)
                    ShowFileNamesWarning();
                else
                    current.Text = openFileDialog.FileName;

                openFileDialog.FileName = null;
            }
        }

        private void ShowFileNamesWarning()
        {
            MessageBox.Show("Файлы сообщения и подписи должны быть разными.\nБыло восстановлено прежнее значение.",
                "Преудпреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private int[] GeneratePrimes(int minValue, int maxValue)
        {
            throw new NotImplementedException();
        }

        private void PrintSettings()
        {
            txtP.Text = cipherer.P.ToString();
            txtQ.Text = cipherer.Q.ToString();

            PrintKeys();
        }

        private void UnlockKeySettings()
        {
            var text = "При ручном вводе программа не проверяет, является ли введенное значением простым числом.\n\n" +
                "Использование сложных чисел может привести к ошибкам в работе программы.\n\n" +
                "Продолжить?";

            var caption = "Предупреждение";

            if (MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                txtE.ReadOnly = false;
                txtD.ReadOnly = false;
                txtN.ReadOnly = false;

                btnUnlockSettings.Text = "Сохранить изменения";
                manualMode = true;
            }
        }

        private void PrintKeys()
        {
            txtE.Text = cipherer.E.ToString();
            txtN.Text = cipherer.N.ToString();
            txtD.Text = cipherer.D.ToString();
        }

        private void InitializeManualMode()
        {
            manualMode = false;
        }

        private void ToggleManualMode()
        {
            if (manualMode)
                LockKeySettings();
            else
                UnlockKeySettings();
        }

        private void LockKeySettings()
        {
            UpdateKeys();

            txtE.ReadOnly = true;
            txtD.ReadOnly = true;
            txtN.ReadOnly = true;

            btnUnlockSettings.Text = "Изменить ключи";
            manualMode = false;
        }

        private void CreateSign()
        {
            if (CheckFiles())
            {
                try
                {
                    byte[] bytes = File.ReadAllBytes(txtMessageFileName.Text);
                    bytes = cipherer.CreateSign(bytes);
                    File.WriteAllBytes(txtSignFileName.Text, bytes);

                    MessageBox.Show("Подпись успешно создана", "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception e)
                {
                    ShowErrorMessage(e.Message);
                }
            }
        }

        private void CompareSign()
        {
            if (CheckFiles())
            {
                try
                {
                    byte[] message = File.ReadAllBytes(txtMessageFileName.Text);
                    byte[] sign = File.ReadAllBytes(txtSignFileName.Text);

                    if (cipherer.CompareSign(message, sign))
                        MessageBox.Show("Проверка подлинности пройдена", "Оповещение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Проверка подлинности не пройдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ShowErrorMessage(string exceptionMessage)
        {
            MessageBox.Show("Во время выполнения операции произошла ошибка:\n" + exceptionMessage,
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool CheckFiles()
        {
            bool cipherNull = string.IsNullOrEmpty(txtSignFileName.Text);
            bool messageNull = string.IsNullOrEmpty(txtMessageFileName.Text);

            if (cipherNull && messageNull)
            {
                MessageBox.Show("Не указаны файлы для обработки", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (cipherNull)
            {
                MessageBox.Show("Не указан файл подписи", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (messageNull)
            {
                MessageBox.Show("Не указан файл сообщения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}