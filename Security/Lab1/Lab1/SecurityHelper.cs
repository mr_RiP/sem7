﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Management;
using Microsoft.Win32;

namespace firstapp
{
    public static class SecurityHelper
    {
        private const string subKey = @".DEFAULT\Software\InformationSecurityLab1";

        public static string GetHash(string to)
        {
            SHA256 hasher = SHA256.Create();
            byte[] hash = hasher.ComputeHash(Encoding.Default.GetBytes(to));
            StringBuilder stringBuilder = new StringBuilder();
            foreach(var item in hash)
                stringBuilder.Append(item.ToString("x2"));

            return stringBuilder.ToString();
        }

        public static RegistryKey OpenSubKey()
        {
            return Registry.Users.OpenSubKey(subKey);
        }

        public static RegistryKey CreateSubKey()
        {
            return Registry.Users.CreateSubKey(subKey);
        }

        public static void DeleteSubKey()
        {
            Registry.Users.DeleteSubKeyTree(subKey);
        }

        public static string GetId()
        {
            string codeName = Environment.MachineName;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
            ManagementObjectCollection hdds = searcher.Get();
            foreach (ManagementObject hdd in hdds)
                if (hdd["SerialNumber"] != null)
                    codeName += " " + hdd["SerialNumber"].ToString().Trim();
            return GetHash(codeName);
        }

        public static bool ValidateSecurityKey()
        {
            string id = GetId();
            RegistryKey key = OpenSubKey();

            return key != null && key.GetValue("id") != null && key.GetValue("id").ToString() == id;
        }
    }
}
