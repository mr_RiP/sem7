﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;
using System.Management;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Windows.Forms; 

namespace firstapp
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
            RegistryKey key = SecurityHelper.CreateSubKey();
            string id = SecurityHelper.GetId();
            key.SetValue("id", id);
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);
            SecurityHelper.DeleteSubKey();
        }

        public Installer()
        {
            InitializeComponent();
        }

    }
}
